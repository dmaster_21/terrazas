<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCondicionToReservacions extends Migration
{
     
    public function up()
    {
       Schema::table('reservacions', function (Blueprint $table) {            
         $table->integer('condicion')->default(1);


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservacions', function (Blueprint $table) {
            
         $table->dropColumn('condicion');

        });
    }
}
