<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVenuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gestion_espacios', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id');
            $table->bigInteger('zona_id');
            $table->bigInteger('horario_id')->nullable(true);
            $table->string('Titulo');
            $table->longText('descripcion');
            $table->longText('direccion');
            $table->integer('capacidad');
            $table->bigInteger('tamaño')->nullable(true);
            $table->string('telefono_lugar')->nullable(true);
            $table->longText('reglamento');
            $table->string('servicios')->nullable(true);
            $table->string('imagen')->nullable(true);
            $table->string('poplet1')->nullable(true);
            $table->bigInteger('latitud')->nullable(true);
            $table->bigInteger('longitud')->nullable(true);
            $table->bigInteger('aprobado')->nullable(true);
            $table->string('serviciosextra')->nullable(true);
            $table->string('tipo');
            $table->boolean('habilitado')->default(true);
            $table->boolean('destacado')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gestion_espacios');
    }
}
