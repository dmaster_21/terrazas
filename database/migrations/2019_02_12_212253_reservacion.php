<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Reservacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('reservacions', function (Blueprint $table) {
            $table->increments('id');            
            $table->integer('venue_id');
            $table->integer('horario_id');
            $table->integer('user_id');
            $table->timestamp('fecha_reservada');
            $table->string('formapago');
            $table->integer('codigopago')->nullable();
            $table->timestamp('fechaPago');
            $table->double('costo_reserva',15,2);
            $table->string('status')->default('apartado');
            $table->boolean('condpago')->default(true);
            $table->string('metadata')->nullable();
            $table->string('tokens')->default(md5(uniqid(rand(), true)));
             $table->boolean('habilitado')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     Schema::dropIfExists('reservacions');    }
}
