<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; //línea necesaria

class Reservacion extends Model
{
    
	use SoftDeletes;


    protected $table = 'reservacions';
    protected $dates = ['deleted_at'];
	protected $fillable = ['venue_id','horario_id','user_id','fecha_reservada', 'formapago','codigopago','fechaPago','costo_reserva','status','metadata','tokens'];

	public function user(){
		   return $this->hasOne('App\User','id','user_id');
	}
	public function horarios(){
		return $this->belongsTo('App\Horario');
	}
	public function venue(){
		return $this->belongsTo('App\Models\GestionEspacios');
	}
	public function rate(){
	   return $this->hasOne('App\Rating');
	 }
	 public function abono(){
	   return $this->hasOne('App\Abono');
	 }
	 public function grupo(){
		return $this->belongsTo('App\Grupo');
	}
}
