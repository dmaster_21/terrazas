<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Mail\Mailable;
use App\Mail\CancelaReserva;
use App\Mail\ContactoEspera;
use Mail;
use App\User;
use App\Reservacion;
use App\Horario;

class CancelarReserva extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cancelar:reserva';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando para Cancelar La Reserva que fue pagada en efectivo Y no sea Reportada';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       $fa=date('Y-m-d h:m:s');
       $fr=date("Y-m-d h:m:s",strtotime($fa."-2 days"));
       
         //voy a buscar las reservaciones que fueron ejecutadas y no han sido reportadas hace 24 horas ;
            $reserva=Reservacion::whereBetween('created_at', array($fr, $fa))->where('condicion',1)->get();         
            foreach ($reserva as $reservas) {
              //busco el horario al cuar peretece la reserva para liberarla
            $horario=Horario::find($reservas->horario_id);
            $horario->apartador='';
            $horario->nota='';
            $horario->status='disponible';
            $horario->save();

            //notificar a los usuarios en espera
            $espera=Reservacion::where('horario_id',$reservas->horario_id)->where('condicion',2)->get();
            foreach ($espera as $userEspera) {
               Mail::to($userEspera->user->email)->send(new ContactoEspera($userEspera));
            }


             
             Mail::to($reservas->user->email)->send(new CancelaReserva($reservas));
            $reservas->delete();//elimino
            }

            

      
    }
}
