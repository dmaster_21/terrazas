<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Mail\Mailable;
use App\Mail\ClienteMail;
use Mail;
use App\User;
use App\Reservacion;
use App\Horario;
class ContactoCliente extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'contacto:cliente';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mensaje Automatico para el cliente una vez Pasada La Reserva';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $to = date("Y-m-d");// fecha actual del servidor 
      $from=date("Y-m-d",strtotime($to."-1 days"));  //le resto un dia   
      //verifico las rentas que se hicieron el dia anterior
      $reserva=Reservacion::where('fecha_reservada',$from)->where('condicion',4)->get(); 
     //cl dd($reserva); 

       foreach ($reserva as $reservas) {
       //envio de email al usuario que su renta culmino       
       Mail::to($reservas->user->email)->send(new ClienteMail($reservas));
        }

     
    }
}
