<?php

namespace App\Console\Commands;
use App\Mail\SendMailable;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\Horario;
class ValorarLugar extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'valorar:lugar';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envio de Email Para Valorar Lugar';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()

    {	
    	$fecha = date('Y-m-d');
       $nuevafecha = strtotime ( '-1 day' , strtotime ($fecha));
        $lugar=1;/*Horario::where('fecha_reserva',$nuevafecha);*/

         Mail::to('yenderson_09_@hotmail.com')->send(new SendMailable($lugar));        
        


    }
}
