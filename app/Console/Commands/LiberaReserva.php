<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Mail\Mailable;
use App\Mail\PeticionCall;
use Mail;
use App\User;
use App\Reservacion;
use App\Horario;
class LiberaReserva extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'liberar:reserva';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Liberar Reserva Que No Ha Sido Cancelada';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       /*aqui busco todos las reservas */
       $reserva=Reservacion::where('condicion',1)->get();
       foreach($reserva as $reservas){ 
             
        $reservas->delete(); 
       }
       
    }
}
