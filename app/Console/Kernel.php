<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
      
        'App\Console\Commands\LiberaReserva',
        'App\Console\Commands\ContactoCliente',
        'App\Console\Commands\CancelarReserva',
        'App\Console\Commands\LiberarApartado'

    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
       $schedule->command('liberar:reserva')->hourly();
       $schedule->command('contacto:cliente')->daily();;
       $schedule->command('cancelar:reserva')->hourly();
       $schedule->command('liberar:apartado')->hourly();

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
