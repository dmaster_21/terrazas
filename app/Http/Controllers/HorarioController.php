<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Horario;
use App\Venue;
use App\Models\GestionEspacios;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class HorarioController extends Controller
{
    public function store(Request $request)
    {
          $request->validate([
            'venue_id' => 'required',           
            'fecha' => 'required',          
            'hora_inicio' => 'required',
            'hora_fin'=>'required',           
            'precio'=>'required|regex:/^[0-9 ,.\'-]+$/i',
        ]);
          
    	$horario = new Horario($request->all());
    	$venue=GestionEspacios::find($request->venue_id);

        $verificar=Horario::where('venue_id',$request->venue_id)->where('fecha',$request->fecha)->get();



        if(count($verificar)>0){
          Session::flash('mensaje', 'Estimado Usuario Para esta Fecha Ya Usted Tiene el Lugar  Publicado');
            Session::flash('class', 'danger');
            return redirect()->intended(url('/fechas'));

        }


        $horario->nombre=$venue->Titulo;
        $horario->tipo=$venue->tipo;
    	$horario->zona=$venue->zona;
    	$horario->capacidad=$venue->capacidad;
    	$horario->user_id=$venue->user_id;


		//guardar
        if ($horario->save()) {
            Session::flash('mensaje', 'Fecha publicada con exito.');
            Session::flash('class', 'success');
            return redirect()->intended(url('/fechas'));
            
        }
        else{
            Session::flash('mensaje', 'Hubó un error, por favor, verifica la información.');
            Session::flash('class', 'danger');
            return redirect()->intended(url('/fechas'))->withInput();
        }

    }



    public function destroy(Request $request){
	      $horario = Horario::find($request->eliminar);
          $horario->habilitado=false;
	      $horario->save();
	      Session::flash('mensaje', 'Horario eliminado con éxito.');
        Session::flash('class', 'success');
        return redirect()->intended(url('/fechas/'))->withInput();
    }


    public function update(Request $request){

           $request->validate([
            'venue_id' => 'required',           
            'fecha' => 'required',          
            'hora_inicio' => 'required',
            'hora_fin'=>'required',           
            'precio'=>'required|regex:/^[0-9 ,.\'-]+$/i',
        ]);

        $verificar=Horario::where('venue_id',$request->venue_id)->where('fecha',$request->fecha)->get();
        if(count($verificar)>0){
          Session::flash('mensaje', 'Estimado Usuario Usted No puede Duplicar el Lugar En la misma Fecha');
        Session::flash('class', 'danger');
        return redirect()->intended(url('/fechas/'))->withInput();
        }
        $horario = Horario::find($request->id);        
        $venue=GestionEspacios::find($request->venue_id);
        $horario->nombre=$venue->Titulo;
		$horario->fecha=$request->fecha;
		$horario->hora_inicio=$request->hora_inicio;
		$horario->hora_fin=$request->hora_fin;
		$horario->precio=$request->precio;
		$horario->venue_id=$request->venue_id;
        

    	$horario->tipo=$venue->tipo;
    	$horario->zona=$venue->zona;
    	$horario->capacidad=$venue->capacidad;
    	$horario->user_id=$venue->user_id;

		//guardar
        if ($horario->save()) {
            Session::flash('mensaje', 'Fecha actualizada con exito.');
            Session::flash('class', 'success');
            return redirect()->intended(url('/fechas/'));
        }
        else{
            Session::flash('mensaje', 'Hubo un error, por favor, verifica la información.');
            Session::flash('class', 'danger');
            return redirect()->intended(url('/fechas/'))->withInput();
        }
    }


    public function api(){
      
        $horario=Horario::all();
       foreach($horario as $horarios){
           $data = array(
            'id' => $horarios->id,
            'title'=>$horarios->nombre,
            'start'=> '2019-02-03',
            'end'=>'2019-02-03',
            'url'=>'eventos/'.$horarios->id
        );

        }


         return  response()->json($data); //para luego retornarlo y estar listo para consumirlo
     }

}
