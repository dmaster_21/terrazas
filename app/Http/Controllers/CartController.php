<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\GestionEspacios;
use App\Horario;
use App\Reservacion;
class CartController extends Controller
{
    //
    public function __construct(){

	//verifico si no existe la variable de session y si no existe la creo

	if(!\Session::has('cart')){
		\Session::put('cart',array());
	} 

}

//show cart
public function show(){

	$cart=\Session::get('cart');//identifico mi array
	$total=$this->total();
	
	
	return view('reservar-lugar',compact('cart','total'));
}


//add items
public function add(Request $request){
	 $request->validate([
            'fecha' => 'required',        
            
        ]);
	 if($request->tip==1){/*reservar espacios y pagar*/
	
	$reserva=Horario::where('venue_id',$request->venue)->where('fecha',$request->fecha)->where('status','=','disponible')->first();

	if($reserva){
	
	$cart=\Session::get('cart');//declaro session local

	$cart[$reserva->id]=$reserva;

	\Session::put('cart',$cart);//actualizo session global

	return redirect()->route('cart.show');
  }else{

  	 Session::flash('mensaje', 'Estimado Usuario Para El dia:'. $request->fecha .' El Lugar No Se Encuentra Disponible Por Favor Intente En Las Próximas Fechas.');
     Session::flash('class', 'danger');       
    
  	return back()->withInput();
  }
}else if($request->tip==2){

	$aparta=Horario::where('venue_id',$request->venue)->where('fecha',$request->fecha)->where('status','<>','rentado')->first();
	

	if($aparta){
    $reserv=Reservacion::where('horario_id',$aparta->id)->get();
	if(count($reserv)>0){
	Session::flash('mensaje', 'Estimado Usuario Para El dia:'. $request->fecha .' El Lugar Cuenta con ('.count($reserv).') Solicitudes de Apartado En Caso de que Algúna Solicitud se decline Usted Será Contactado Para Finiquitar El Proceso de La reservacón Del Lugar');
     Session::flash('class', 'danger'); 

	}else{

		Session::flash('mensaje', 'Estimado Usuario Para El dia:'. $request->fecha .' Usted es El Unico en Apartar El Lugar Le Recomendamos Finiquitar el Proceso en Un Lapso no mayor a 24 HORAS de lo contrario declinaremos su solicitud.');
             Session::flash('class', 'success');
 }
	return view('apartar',compact('aparta','reserv'));
   }else{

   	Session::flash('mensaje', 'Estimado Usuario Para El dia:'. $request->fecha .' El Lugar No Se Encuentra Disponible  Por Favor Intente En Las Próximas Fechas.');
     Session::flash('class', 'danger');       
    
  	return back()->withInput();
   }

}


}



public function delete($id){

	$cart=\Session::get('cart');//declaro session local
	unset($cart[$id]);///inteccion de dependencias
	\Session::put('cart',$cart);//cambio sesion global

	return redirect()->route('cart.show');


}



//trash cart
public function trash(){
	\Session::forget('cart');
    return redirect()->route('cart.show');

	}

//Total

	private function total(){

			$cart=\Session::get('cart');//declaro sesseion local
			$total=0;
			foreach ($cart as $item) {
				$total+= $item->precio;
			}

			return $total;

	}

}
