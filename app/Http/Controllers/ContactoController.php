<?php

namespace App\Http\Controllers;
// necesitamos usar esta clase para poder enviar correos.
use Mail; 
// y tambien la que hemos creado para enviarle los datos
use App\Mail\Contacto; 
use Illuminate\Http\Request;

class ContactoController extends Controller
{
    //
    public function enviar(Request $request)
    {
    	//dd($request->all());

        $request->validate([
            'nombre' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'mensaje'=>'required|string|min:50',
            'phone'=>'required|string|min:8'
        ]);

        $forminput = [
            'nombre' => $request->nombre,
            'email' => $request->email,
            'mensaje' =>$request->mensaje,
            'phone'=>$request->phone
        ];

        		/*
        * No olvides cambiar el correo aquí. 
        * Este es el correo donde vas a recibir 
        * los mensajes.
        **/
        Mail::to(env('MAIL_USERNAME'))->send(new Contacto($forminput));
         return redirect('/')->with('mensaje', '¡Mensaje enviado! Gracias por contactarnos.')->with('class', 'success');
    }
}
