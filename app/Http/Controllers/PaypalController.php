<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use App\Horario;
use App\Reservacion;
use App\User;
class PaypalController extends Controller
{

	private $_api_context;



	public function __construct(){

    if(!\Session::has('idtoken')){
		\Session::put('idtoken');
	} 

   //configuracion api context paypal
	$paypal_conf=\Config::get('paypal');
	$this->_api_context=new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'],$paypal_conf['secret']));
	$this->_api_context->setConfig($paypal_conf['settings']);
	}

	public function postPayment(){
		$payer= new Payer();
		$payer->setPaymentMethod('paypal');
		$items = array();
		$currency='MXN';

//creacion del objeto determinado
		 $cart=\Session::get('cart');
		 $subtotal=0;
        foreach($cart as $lugar){
		$item=new item();
		$item->setName($lugar->nombre)
		->setCurrency($currency)
		->setDescription('Renta De Lugares y Espacios')
		->setQuantity('1')
		->setPrice($lugar->precio);
	

		$items[] = $item;
		$subtotal+=$lugar->precio;
   }
		$item_list= new ItemList();
		$item_list->setItems($items);

		$details = new Details();
		$details->setSubtotal($subtotal)
		->setShipping(0);

 
		$total = $subtotal ;

		$amount= new Amount();
		$amount->setCurrency($currency)//moneda
		->setTotal($total)
		->setDetails($details);
		

		//transaccion
		$transaction =new Transaction();
		$transaction->setAmount($amount)
		->setItemList($item_list)
		->setDescription('Renta del Lugar Mediante Plataforma Electrónica');

		$redirect_urls= new RedirectUrls();
		$redirect_urls->setReturnUrl(\URL::route('payment.status'))
		->setCancelUrl(\URL::route('payment.status'));
		//aqui se ejecuta el pago
		
		$payment = new Payment();
		$payment->setIntent('Sale')
			->setPayer($payer)
			->setRedirectUrls($redirect_urls)
			->setTransactions(array($transaction));

		/*******************************/

		try {
			$payment->create($this->_api_context);
		} catch (\PayPal\Exception\PPConnectionException $ex) {
			if (\Config::get('app.debug')) {
				echo "Exception: " . $ex->getMessage() . PHP_EOL;
				$err_data = json_decode($ex->getData(), true);
				exit;
			} else {
				die('Ups! Algo salió mal');
			}
		}
		foreach($payment->getLinks() as $link) {
			if($link->getRel() == 'approval_url') {
				$redirect_url = $link->getHref();
				break;
			}
		}
		// add payment ID to session
		\Session::put('paypal_payment_id', $payment->getId());
		if(isset($redirect_url)) {
			// redirect to paypal
			return \Redirect::away($redirect_url);
		}
		return \Redirect::route('cart-show')
			->with('error', 'Ups! Error desconocido.');


	}
    

    	public function getPaymentStatus(Request $request)
	{


		// Get the payment ID before session clear
		$payment_id = $request->paymentId;
		//dd($payment_id);
		// clear the session payment ID
		\Session::forget('paypal_payment_id');
		
		$payerId =$request->PayerID;// \Input::get('PayerID');
		$token =$request->token;// \Input::get('token');
		//if (empty(\Input::get('PayerID')) || empty(\Input::get('token'))) {
		if (empty($payerId) || empty($token)) {
			return \Redirect::route('cart.show')->with('mensaje','Hubo un problema al intentar pagar con Paypal')->with('class', 'danger');
		}
		$payment = Payment::get($payment_id, $this->_api_context);
		// PaymentExecution object includes information necessary 
		// to execute a PayPal account payment. 
		// The payer_id is added to the request query parameters
		// when the user is redirected from paypal back to your site
		$execution = new PaymentExecution();
		$execution->setPayerId($request->PayerID);
		//Execute the payment
		$result = $payment->execute($execution, $this->_api_context);
		//echo '<pre>';print_r($result);echo '</pre>';exit; // DEBUG RESULT, remove it later
		if ($result->getState() == 'approved') { // payment made			

		$cart=\Session::get('cart');
		$rand_part = str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789".uniqid());

        foreach($cart as $item){
        $user=User::find(auth()->id());

        $horario=Horario::find($item->id);
        $horario->status='rentado';
        $horario->apartador=$user->name;
        $horario->save();


      $reserv=new Reservacion();
        $reserv->venue_id=$item->venue_id;
        $reserv->horario_id=$item->id;
        $reserv->user_id=auth()->id();
        $reserv->fecha_reservada=$item->fecha;
        $reserv->formapago='Paypal';
        $reserv->fechaPago=now();    
        $reserv->status='rentado';      
        $reserv->costo_reserva=$item->precio;
        $reserv->condpago=true;
        $reserv->tokens=$rand_part;
        $reserv->condicion=4;//renta procesada exitosamente

        $reserv->save();
        }

			//$this->saveOrder(\Session::get('cart'));
			\Session::forget('cart');
			return \Redirect::route('reserva.client')->with('mensaje','Estimado Usuario Tu Reservación se Realizo de manera Exitosa')->with('class', 'success');
			
		}
		return \Redirect::route('reserva.client')->with('mensaje','eL Pago fue Cancelado')->with('class', 'danger');
	}

/**********************************AQUI SE PAGA EL APARATADO*******************************************/

public function Paymentapartado($tokens){
	///dd($tokens);
	$idtoken=\Session::get('idtoken');//declaro session local
	$idtoken=$tokens;
	\Session::put('idtoken',$idtoken);//actualizo session global



	     $payer= new Payer();
		$payer->setPaymentMethod('paypal');
		$items = array();
		$currency='MXN';

        //creacion del objeto determinado
		$cart=Reservacion::where('tokens',$tokens)->where('user_id',auth()->id())->where('habilitado',1)->get();

		 $subtotal=0;
        foreach($cart as $lugar){
		$item=new item();
		$item->setName($lugar->venue->Titulo)
		->setCurrency($currency)
		->setDescription('Renta De Lugares y Espacios')
		->setQuantity('1')
		->setPrice($lugar->costo_reserva);
	

		$items[] = $item;
		$subtotal+=$lugar->costo_reserva;
   }
		$item_list= new ItemList();
		$item_list->setItems($items);

		$details = new Details();
		$details->setSubtotal($subtotal)
		->setShipping(0);

 
		$total = $subtotal ;

		$amount= new Amount();
		$amount->setCurrency($currency)//moneda
		->setTotal($total)
		->setDetails($details);
		

		//transaccion
		$transaction =new Transaction();
		$transaction->setAmount($amount)
		->setItemList($item_list)
		->setDescription('Renta del Lugar Mediante Plataforma Electrónica');

		$redirect_urls= new RedirectUrls();
		$redirect_urls->setReturnUrl(\URL::route('payment.statusv2'))
		->setCancelUrl(\URL::route('payment.statusv2'));
		//aqui se ejecuta el pago
		
		$payment = new Payment();
		$payment->setIntent('Sale')
			->setPayer($payer)
			->setRedirectUrls($redirect_urls)
			->setTransactions(array($transaction));

		/*******************************/

		try {
			$payment->create($this->_api_context);
		} catch (\PayPal\Exception\PPConnectionException $ex) {
			if (\Config::get('app.debug')) {
				echo "Exception: " . $ex->getMessage() . PHP_EOL;
				$err_data = json_decode($ex->getData(), true);
				exit;
			} else {
				die('Ups! Algo salió mal');
			}
		}
		foreach($payment->getLinks() as $link) {
			if($link->getRel() == 'approval_url') {
				$redirect_url = $link->getHref();
				break;
			}
		}
		// add payment ID to session
		\Session::put('paypal_payment_id', $payment->getId());
		if(isset($redirect_url)) {
			// redirect to paypal
			return \Redirect::away($redirect_url);
		}
		return \Redirect::route('reserva.client')->with('mensaje','Error DESCONOCIDO')->with('class', 'danger');

}

/*ejecutamos pago apartado*/
	public function getPaymentStatusv2(Request $request)
	{


		// Get the payment ID before session clear
		$payment_id = $request->paymentId;
		//dd($payment_id);
		// clear the session payment ID
		\Session::forget('paypal_payment_id');
		
		$payerId =$request->PayerID;// \Input::get('PayerID');
		$token =$request->token;// \Input::get('token');
		//if (empty(\Input::get('PayerID')) || empty(\Input::get('token'))) {
		if (empty($payerId) || empty($token)) {
			return \Redirect::route('reserva.client')->with('mensaje','Ha Ocurrido Problemas AL Intentar Realizar el Pago ')->with('class', 'danger');
		}
		$payment = Payment::get($payment_id, $this->_api_context);
		// PaymentExecution object includes information necessary 
		// to execute a PayPal account payment. 
		// The payer_id is added to the request query parameters
		// when the user is redirected from paypal back to your site
		$execution = new PaymentExecution();
		$execution->setPayerId($request->PayerID);
		//Execute the payment
		$result = $payment->execute($execution, $this->_api_context);
		//echo '<pre>';print_r($result);echo '</pre>';exit; // DEBUG RESULT, remove it later
		if ($result->getState() == 'approved') { // payment made			

		$idtoken=\Session::get('idtoken');//token declarado en la sesion

		$reserva=Reservacion::where('tokens',$idtoken)->where('user_id',auth()->id())->where('habilitado',1)->get();

		//* verificamos que alquien no procesara el pago**//
	   	  foreach ($reserva as $resultado) {
        $info=Reservacion::where('horario_id',$resultado->horario_id)->where('status','rentado')->where('condicion',4)->get();

       if(count($info)>0){
        
          return \Redirect::route('reserva.client')->with('mensaje','Estimado Usuario te Informamos que el Lugar Ya Se Encuentra Rentado Te Invitamos A Ver Fechas Disponibles')->with('class', 'danger');
      }
    }
		
     	
       foreach($reserva as $item){
        $user=User::find(auth()->id());
        $horario=Horario::find($item->horario_id);
        $horario->status='rentado';
        $horario->apartador=$user->name;
        $horario->save();


        $reserv=Reservacion::find($item->id);        
        $reserv->formapago='Paypal';
        $reserv->fechaPago=now();    
        $reserv->status='rentado';
        $reserv->condpago=true;
        $reserv->condicion=4;//renta procesada exitosamente
        $reserv->save();
        //listo
        }
   

			//$this->saveOrder(\Session::get('cart'));
			\Session::forget('idtoken');
			return \Redirect::route('reserva.client')->with('mensaje','Estimado Usuario Tu Reservación se Realizo de manera Exitosa')->with('class', 'success');
      
		}
		return \Redirect::route('reserva.client')->with('mensaje','El Pago fue Cancelado')->with('class', 'danger');
		
	}










}
