<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Reservacion;
use App\Horario;
use App\User;
use App\Models\GestionEspacios;
class ReservasController extends Controller
{
   
    public function store(Request $request)
    {
        //
     //   dd($request->all());
        $request->validate(['metodo' => 'required', ]);
        $rand_part = str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789".uniqid());
        if($request->metodo=='Efectivo'){
        $cart=\Session::get('cart');

        foreach($cart as $item){
        $user=User::find(auth()->id());

        $horario=Horario::find($item->id);
        $horario->status='apartado';
        $horario->apartador=$user->name;
        $horario->save();

    
        $reserv=new Reservacion();
        $reserv->venue_id=$item->venue_id;
        $reserv->horario_id=$item->id;
        $reserv->user_id=auth()->id();
        $reserv->fecha_reservada=$item->fecha;
        $reserv->formapago='Efectivo';
        $reserv->fechaPago=now();       
        $reserv->costo_reserva=$item->precio;
        $reserv->condpago=false;
        $reserv->status='apartado';
        $reserv->tokens=$rand_part;
        $reserv->condicion=3;//pago Efectivo espera confirmar



        $reserv->save();
        }
        \Session::forget('cart');



        ///redireccionamos al Cliente
        Session::flash('mensaje', 'Eltimado Usuario Su Solicitud se Proceso de Manera Exitosa, Usted Posee 24 Horas Para Reportar su pago de Lo contrario el sistema Anulara su solicitud dando Prioridad A los clientes en Lista de Espera');
         Session::flash('class', 'success');
         return redirect()->intended(url('/reservaClient'));

        }else if($request->metodo=='Paypal'){
          
          return redirect('payment');
        
        }else{
        
        $user=User::find(Auth::user()->id);
        $cart=\Session::get('cart');//declaro sesseion local
            $total=0;
            foreach ($cart as $item) {
                $total+= $item->precio;
          }

          

         return view('pagar',['user'=>$user,'costo'=>$total,'forma'=>'rentado','tokens'=>'']);
/*
         Session::flash('mensaje', 'La plataforma OpenPay Aún No Se Encuentra Operativa Estamos Trabajando Para Brindarles Un Mejor Servicio.');
         Session::flash('class', 'danger');
          return redirect()->intended(url('/cart/show'));*/
        }


    }


      public function apartar(Request $request){
        
        $user=User::find(auth()->id());
        $horario=Horario::find($request->horario_id);

        /*verifico si para este dia el cliente no reservo*/
        $ClientR=Reservacion::where('horario_id',$request->horario_id)->where('user_id',auth()->id())->get();
        //dd(count($ClientR));

         $rand_part = str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789".uniqid());

       // dd(count($ClientR));

        //valido si el lugar aun no ha sido Apartado para colocar el apartador
       if(count($ClientR)>=1){
           Session::flash('mensaje', 'Estimado Usuario. su Solicitud No Puso Ser Procesada debido a que Ya Usted Posee Una Solicitd Para Este Lugar en la Fecha Solicitada.!');
            Session::flash('class', 'danger');
            return redirect()->intended(url('/reservaClient'));      

       }else{
        if($horario->status=='disponible'){
        $horario->status='apartado';
        $horario->apartador=$user->name;
        $horario->save();
        $condicion='1';//genero apartado espontaneo
        }else{
        $condicion='2';//los usurios entran en espera
        }

     
        $reserv=new Reservacion();
        $reserv->venue_id=$horario->venue_id;
        $reserv->horario_id=$horario->id;
        $reserv->user_id=auth()->id();
        $reserv->fecha_reservada=$horario->fecha;
        $reserv->formapago='N/A';
        $reserv->fechaPago=now();
        $reserv->status='apartado';
        $reserv->costo_reserva=$horario->precio;
        $reserv->condpago=false;
        $reserv->tokens=$rand_part;
        $reserv->condicion=$condicion;//renta procesada exitosamente

        if($reserv->save()){
            \Session::forget('cart');
            /*el lugar fue apartando*/
            Session::flash('mensaje', 'Estimado Usuario El Lugar Ha Sido Apartado de Manera Satisfactoria.!');
            Session::flash('class', 'success');
            return redirect()->intended(url('/reservaClient'));
        }else{

             Session::flash('mensaje', 'Disculpe Su Solicitud No Pudo ser Procesada. Intente Luego');
            Session::flash('class', 'danger');

            return back();
        }
    }

  }



 public function reserva_directa(Request $request){

    $request->validate([

        'cliente' => 'required',
        'nota' => 'required',
        'condicion' => 'required'
    ]);

    $reserva=Horario::find($request->id);
    $reserva->apartador=$request->cliente;
    $reserva->nota=$request->nota;
    $reserva->status=$request->condicion;

   if($reserva->save()){
     Session::flash('mensaje', 'La Reserva Directa para el Cliente '.$request->cliente.' Ha sido Creada de Manera Satisfactoria!');
   Session::flash('class', 'success');
           
   }else{

    Session::flash('mensaje','Error de Registro Contacte al administrador del Ssitema.');
    Session::flash('class', 'danger');


   }

    return redirect()->intended(url('/fechas'));



 }


 public function reservadetalle(){



    $lugar=GestionEspacios::where('user_id',auth()->id())->where('habilitado','1')->get();
    $detalle="";


    return view('admin.reservaverificar',compact('lugar','detalle'));

 }

 public function reservavista(Request $request){
     $request->validate([

        'fecha' => 'required',
        'lugar' => 'required',
       
    ]);

   
   $lugar=GestionEspacios::where('user_id',auth()->id())->where('habilitado','1')->get();
    $detalle=Horario::where('fecha',$request->fecha)->where('venue_id',$request->lugar)->first();
   
   if($detalle){
    return view('admin.detallelibera',compact('lugar','detalle'));
   }else{
    return view('admin.reservaverificar',compact('lugar','detalle'));
  }
 }


 public function editarReserva(Request $request){

   $request->validate([

        'cliente' => 'required',
        'nota' => 'required',
        'condicion' => 'required'
    ]);

    $reserva=Horario::find($request->id);
    $reserva->apartador=$request->cliente;
    $reserva->nota=$request->nota;
    $reserva->status=$request->condicion;

   if($reserva->save()){
     Session::flash('mensaje', 'La Edición ha sido ejecutada de Manera Satisfactoria!');
   Session::flash('class', 'success');
           
   }else{

    Session::flash('mensaje','Error de Registro Contacte al administrador del Ssitema.');
    Session::flash('class', 'danger');


   }

   return back();
 }


  public function completar($tokens){
    $reserva=Reservacion::where('tokens',$tokens)->where('user_id',auth()->id())->where('habilitado',1)->get();
  
/*aqui valido que al momento del usuario reportar la rserva otra persona no la haya creado*/
    foreach ($reserva as $resultado) {
      $info=Reservacion::where('horario_id',$resultado->horario_id)->where('status','rentado')->where('condicion',4)->get();

      


      if(count($info)>0){
         Session::flash('mensaje', 'Estimado Usuario te Informamos que el Lugar Ya Se Encuentra Rentado Te Invitamos A Ver Fechas Disponibles');
         Session::flash('class', 'danger');
          return redirect()->intended(url('/reservaClient'));
      }
    }
    /*end validación*/

    if(count($reserva)<1){
     Session::flash('mensaje', 'Disculpe Su solicitud es Invalida');
         Session::flash('class', 'danger');
    return redirect()->intended(url('/reservaClient'));
   }
  
    return view('completo-reserva',compact('reserva','tokens'));
 }


 public function continuarPago(Request $request){

   $reserva=Reservacion::where('tokens',$request->tokens)->where('user_id',auth()->id())->where('habilitado',1)->get();
   
  
  if($request->metodo=='Efectivo'){
       

        foreach($reserva as $item){
    
        $reserv=Reservacion::find($item->id);        
        $reserv->formapago='Efectivo';
        $reserv->fechaPago=now();
        $reserv->condpago=false;
        $reserv->condicion=3;//pago Efectivo espera confirmar
        $reserv->created_at=now();
        $reserv->save();
        }
        ///redireccionamos al Cliente
        Session::flash('mensaje', 'Estimado Usuario Tienes 24 Horas Para Hacer Tu Pago En Efectivo De Manera Personal Lo contrario Tu Reserva Sera Anulada.!!');
         Session::flash('class', 'success');

         return redirect()->intended(url('/reservaClient'));

        }else if($request->metodo=='Paypal'){
          
          return redirect('payment/apartado/'.$request->tokens);
        
        }else{
        
        $user=User::find(Auth::user()->id);
       
            $total=0;
            foreach ($reserva as $item) {
                $total+= $item->costo_reserva;
          }

          

        return view('pagar',['user'=>$user,'costo'=>$total,'forma'=>'apartado','tokens'=>$request->tokens]);
/*
         Session::flash('mensaje', 'La plataforma OpenPay Aún No Se Encuentra Operativa Estamos Trabajando Para Brindarles Un Mejor Servicio.');
         Session::flash('class', 'danger');
          return redirect()->intended(url('/cart/show'));*/
        }


 }


 public function efectivo(Request $request){

  $request->validate([

        'fecha' => 'required',
        'nota' => 'required',
        
    ]);


$reserva=Reservacion::find($request->id_reserva);
$reserva->fechaPago=$request->fecha;
$reserva->status='rentado';
$reserva->formapago='Efectivo';
$reserva->condPago=true;
$reserva->condicion=4;//renta exitosa

$horario=Horario::find($reserva->horario_id);
$horario->nota=$request->nota;
$horario->status='rentado';
$reserva->save();
$horario->save();


Session::flash('mensaje', 'El Pago fue reportado De manera Exitosa.');
Session::flash('class', 'success');

 return redirect()->intended(url('/rentasConfirmar'));


 }
}
