<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Models\GestionEspacios;
use App\User;
use App\Servicio;
use App\Galeria;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Mail\Mailable;
use App\Mail\PeticionCall;
use Mail;

class ProveedoresController extends Controller
{
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'password' => 'min:6|confirmed',
        ]);
    }
    public function store(Request $request){


        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            Session::flash('mensaje', 'Hubo un error al guardar la información.');
            Session::flash('class', 'danger');
            return redirect()->intended(url()->previous())->withInput();
        }
        else{

      // dd($request->all());
            $user = new User();
            $GestionEspacios = new GestionEspacios();
            $user->name=$request->name;
            $user->email=$request->email;
            $user->tel=$request->tel;
            $user->empresa=$request->empresa;
            $user->avatar='img/dummy.png';
            $user->role="proveedor";

            $user->password=bcrypt('$request->password');
            $user->habilitado=0;

            $GestionEspacios->habilitado=0;

            //categoria
            if (isset($request->servicio)) {
              $GestionEspacios->servicios=implode(",", $request->servicio);
            }
            else{
                Session::flash('mensaje', 'Selecciona o crea por lo menos un servicio.');
                Session::flash('class', 'danger');
                return redirect()->intended(url()->previous())->withInput();
            }
            if ($request->hasFile('imagen')) {
              $file = $request->file('imagen');
              if ($file->getClientOriginalExtension()=="jpg" || $file->getClientOriginalExtension()=="png") {
                $name = str_slug($request->nombre, '-') . "-". time()."." . $file->getClientOriginalExtension();
                $path = base_path('uploads/gestions/');
                $file-> move($path, $name);
                $GestionEspacios->imagen = $name;
                }


              else{

                Session::flash('mensaje', 'El archivo no es una imagen valida.');
                Session::flash('class', 'danger');
                return redirect()->intended(url()->previous())->withInput();
              }

            }
            else{
              Session::flash('mensaje', 'El archivo no es una imagen valida.');
              Session::flash('class', 'danger');
              return redirect()->intended(url()->previous())->withInput();
            }

            //guardar
            $user->save();
            $GestionEspacios->user_id=$user->id;
            $GestionEspacios->zona_id=$request->zona_id;
            $GestionEspacios->Titulo=$request->nombre;
            $GestionEspacios->descripcion=$request->descripcion;
            $GestionEspacios->direccion=$request->direccion;
            $GestionEspacios->descripcion=$request->descripcion;
            $GestionEspacios->capacidad=$request->capacidad;
            $GestionEspacios->tamaño=$request->tamaño;
            $GestionEspacios->telefono_lugar=$request->telefono_lugar;
            $GestionEspacios->reglamento=$request->reglamento;
            $GestionEspacios->latitud=$request->latitud;
            $GestionEspacios->longitud=$request->longitud;
            
            $GestionEspacios->aprobado=0;
            $GestionEspacios->tipo=$request->tipo;
            $GestionEspacios->habilitado=0;


            if ($GestionEspacios->save()) {
                Session::flash('mensaje', 'Solicitud enviada.');
                Session::flash('class', 'success');
                
            }
            else{
                Session::flash('mensaje', 'Hubó un error, por favor, verifica la información.');
                Session::flash('class', 'danger');
                return redirect()->intended(url()->previous())->withInput();
            }

            //poplets
            if (!$request->poplets&&$request->hasFile('poplet1')) {
                $request->poplets=1;
            }
            if ($request->poplets) {
                for ($i=1; $i <= intval($request->poplets); $i++) { 
                    $poplet = new Galeria();
                   if ($request->hasFile('poplet'.$i)) {
                      $file = $request->file('poplet'.$i);

                      if ($file->getClientOriginalExtension()=="jpg" || $file->getClientOriginalExtension()=="png") {
                        $name = str_slug($request->nombre, '-') . "-" . $i . "-" . time()."." . $file->getClientOriginalExtension();
                        $path = base_path('uploads/gestions/poplets/'.$GestionEspacios->id.'/');
                        $file->move($path, $name);
                        $poplet->imagen = $name;
                        $poplet->gestion_id = $GestionEspacios->id;
                        $poplet->save();
                        }
                      else{
                        Session::flash('mensaje', 'Hubo un error al guardar la galería.');
                        Session::flash('class', 'danger');
                        return redirect()->intended(url()->previous())->withInput();
                      }

                    }
                }
                
            }

            return redirect()->intended(url('/entrar'))->withInput();
        }
    	
    }
    public function destroy(Request $request){
      $GestionEspacios = GestionEspacios::find($request->eliminar);
      $user=$GestionEspacios->user;
      $user->delete();
      $GestionEspacios->delete();
      Session::flash('mensaje', 'Peticion eliminada con éxito.');
        Session::flash('class', 'success');
        return redirect()->intended(url('/peticiones/'))->withInput();
    }

    public function accept(Request $request){
      $GestionEspacios = GestionEspacios::find($request->aceptar);
      $user=$GestionEspacios->user;
      $user->habilitado=1;
      $user->status='activo';
      $GestionEspacios->habilitado=1;
      $GestionEspacios->aprobado=1;
   
      $user->save();
      $GestionEspacios->save();
        
     Mail::to($user->email)->send(new PeticionCall($user));
      Session::flash('mensaje', 'Peticion aceptada con éxito.');
        Session::flash('class', 'success');
        return redirect()->intended(url('/peticiones/'))->withInput();
    }
}
