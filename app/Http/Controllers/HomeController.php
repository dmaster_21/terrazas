<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
use App\Reservacion;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function admin(Request $request)
    {
        
        $from_n = strtotime ( $request->from )  ;
      $to_n = strtotime ( $request->to )  ;
      $from = date ( 'Y-m-d' , $from_n );
      $to = date ( 'Y-m-d' , $to_n );
        
        $usuarios=User::whereBetween('created_at', array($from, $to))->where('is_admin',0)->where('status','Activo')->count();
        $mujeres=User::whereBetween('created_at', array($from, $to))->where('is_admin',0)->where('status','Activo')->where('genero','Femenino')->count();
        $hombres=User::whereBetween('created_at', array($from, $to))->where('is_admin',0)->where('status','Activo')->where('genero','Masculino')->count();

        
      $totales=Reservacion::whereBetween('created_at', array($from, $to))->where('habilitado',1)->count();
      $rentadas=Reservacion::whereBetween('created_at', array($from, $to))->where('habilitado',1)->where('status','rentado')->count();
      $espera=Reservacion::whereBetween('created_at', array($from, $to))->where('habilitado',1)->where('status','apartado')->count();

      $vistos= DB::table('reservacions')
    ->join('gestion_espacios', 'gestion_espacios.id', '=', 'reservacions.venue_id')
    ->select('gestion_espacios.Titulo',DB::raw('count(reservacions.venue_id) as qty'))
    ->orderBy(DB::raw('count(reservacions.venue_id)'),'DESC')
    ->groupBy('reservacions.venue_id','gestion_espacios.Titulo')
    ->whereBetween('reservacions.created_at', array($from, $to))  
    ->take(5)                 
    ->get();

     $zona=DB::table('gestion_espacios')
    ->join('zonas', 'zonas.id', '=', 'gestion_espacios.zona_id')
    ->select('zonas.nombre',DB::raw('count(gestion_espacios.zona_id) as qty'))
    ->groupBy('gestion_espacios.zona_id','zonas.nombre')
    ->whereBetween('gestion_espacios.created_at', array($from, $to))  
    ->take(15)                 
    ->get();
      
      
    
      return view('admin',compact('usuarios','mujeres','hombres','totales','rentadas','espera','from','to','vistos','zona'));

    }


    public function prov(Request $request)
    {
        
        $from_n = strtotime ( $request->from )  ;
      $to_n = strtotime ( $request->to )  ;
      $from = date ( 'Y-m-d' , $from_n );
      $to = date ( 'Y-m-d' , $to_n );

      $totales= DB::table('reservacions')
    ->join('gestion_espacios', 'gestion_espacios.id', '=', 'reservacions.venue_id')    
    ->whereBetween('reservacions.created_at', array($from, $to)) 
    ->where('gestion_espacios.user_id',auth()->id())                 
    ->count();

    $rentadas= DB::table('reservacions')
    ->join('gestion_espacios', 'gestion_espacios.id', '=', 'reservacions.venue_id')    
    ->whereBetween('reservacions.created_at', array($from, $to)) 
    ->where('gestion_espacios.user_id',auth()->id())  
    ->where('status','rentado')               
    ->count();

    $espera= DB::table('reservacions')
    ->join('gestion_espacios', 'gestion_espacios.id', '=', 'reservacions.venue_id')    
    ->whereBetween('reservacions.created_at', array($from, $to)) 
    ->where('gestion_espacios.user_id',auth()->id())  
    ->where('status','apartado')               
    ->count();

      $vistos= DB::table('reservacions')
    ->join('gestion_espacios', 'gestion_espacios.id', '=', 'reservacions.venue_id')
    ->select('gestion_espacios.Titulo',DB::raw('count(reservacions.venue_id) as qty'))
    ->orderBy(DB::raw('count(reservacions.venue_id)'),'DESC')
    ->groupBy('reservacions.venue_id','gestion_espacios.Titulo')
    ->whereBetween('reservacions.created_at', array($from, $to)) 
    ->where('gestion_espacios.user_id',auth()->id()) 
    ->take(5)                 
    ->get();
      
  
    
        return view('panel',compact('totales','rentadas','espera','from','to','vistos'));
        
       

    }

    public function activacion($token,$email)
    {
        $usuario=User::where('email',$email)->where('token',$token)->where('status','Pendiente')->first();
        if ($usuario) {
            $usuario->status="Activo";
            $usuario->save();
            Session::flash('mensaje', 'Activación exitosa.');
            Session::flash('class', 'success');
        }
        else{
            Session::flash('mensaje', 'Error de activación.');
            Session::flash('class', 'danger');
        }

        return redirect()->intended(url('/entrar'));
    }
}
