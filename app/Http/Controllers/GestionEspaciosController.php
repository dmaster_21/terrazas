<?php

namespace App\Http\Controllers;

use App\Models\GestionEspacios;
use Illuminate\Http\Request;
use App\Galeria;
use App\Zona;
use App\Servicio;
use App\ServicioExtra;
use App\Horario;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
class GestionEspaciosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $user=User::find(auth()->id());

      if($user->role=='admin'){

     $index = GestionEspacios::where('aprobado',1)->where('habilitado',true)->get();
      }else{
        $index = GestionEspacios::where('aprobado',1)->where('user_id','=',auth()->id())->where('habilitado',true)->get();
      }


        
        return view('Gestion_Espacios.index',compact('index'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $zonas=Zona::orderBy('nombre','asc')->where('habilitado',true)->get();
        $servicios = Servicio::where('habilitado',true)->get();
        $serviciosextra = ServicioExtra::get();
        return view('Gestion_Espacios.create',compact('zonas','servicios','serviciosextra'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $gestion = new GestionEspacios();
        
          $request->validate([
            'titulo' => 'required',           
            'descripcion' => 'required',
            'reglamento' => 'required',
            'tipo' => 'required',
            'zona'=>'required',
            'capacidad'=>'required|regex:/^[0-9 ,.\'-]+$/i',
            'latitud'=>'required|regex:/^[0-9 ,.\'-]+$/i',
            'longitud'=>'required|regex:/^[0-9 ,.\'-]+$/i',
            'telefono'=>'required|min:8|max:12|regex:/^[0-9 ,.\'-]+$/i',
            'tamaño'=>'required|regex:/^[0-9 ,.\'-]+$/i',
        ]);

       
       
        $gestion->user_id=$request->user_id;
        $gestion->zona_id=$request->zona;
        $gestion->descripcion=$request->descripcion;
        $gestion->tamaño=$request->tamaño;
        $gestion->reglamento=$request->reglamento;
        $gestion->direccion=$request->direccion;
      
        
       
        //servicios
        
        if (isset($request->servicio)) {
          $gestion->servicios=implode(",", $request->servicio);
        }
        else{
            Session::flash('mensaje', 'Selecciona o crea por lo menos un servicio.');
            Session::flash('class', 'danger');
           // return redirect()->intended(url('/gestions/nuevo'))->withInput();
            return redirect()->intended(url('/gestion_espacios/create'))->withInput();
        }


        //serviciosextra
        if (isset($request->serviciosextra)) {
          $gestion->serviciosextra=implode(",", $request->serviciosextra);
        }
        else{
            $gestion->serviciosextra=null;
        }

       if(count($request->poplet)<=0){

              Session::flash('mensaje', 'Usted debe Ingresar Al menos Una Imagen en Galeria');
              Session::flash('class', 'danger');
               return redirect()->intended(url('/gestion_espacios/create'))->withInput();
        }
         

        //habilitado
     /* if (isset($request->habilitado)) {
            $gestion->habilitado=1;
        }
        else{
            $gestion->habilitado=0;
        }*/
       
        if ($request->hasFile('imagen')) {
          $file = $request->file('imagen');
          if ($file->getClientOriginalExtension()=="jpg" || $file->getClientOriginalExtension()=="png") {
            $name = str_slug($request->nombre, '-') . "-". time()."." . $file->getClientOriginalExtension();
            $path = base_path('uploads/gestions/');
            $file-> move($path, $name);
            $gestion->imagen = $name;
            }


          else{
            Session::flash('mensaje', 'El archivo no es una imagen valida.');
            Session::flash('class', 'danger');
            return redirect()->intended(url('/gestion_espacios/create'))->withInput();
          }

        }
        else{
          Session::flash('mensaje', 'El archivo no es una imagen valida.');
          Session::flash('class', 'danger');
          return redirect()->intended(url('/gestion_espacios/create'))->withInput();
        }
        $req = $request;
        unset($req['poplet']);
        //guardar
        $gestion->Titulo=$request->titulo;
        $gestion->capacidad=$request->capacidad;
        $gestion->user_id=$request->user_id;
        $gestion->zona_id=$request->zona;
        $gestion->descripcion=$request->descripcion;
        $gestion->tamaño=$request->tamaño;
        $gestion->reglamento=$request->reglamento;
        $gestion->direccion=$request->direccion;
        $gestion->tamaño=$request->tamaño;
        $gestion->telefono_lugar=$request->telefono;
        $gestion->latitud=$request->latitud;
        $gestion->longitud=$request->longitud;
        $gestion->aprobado=$request->aprobado;
        //$gestion->servicios=$request->servicio;
        //$gestion->serviciosextra=$request->serviciosextra;
        $gestion->tipo=$request->tipo;
       
        if ($gestion->save()) {
            Session::flash('mensaje', 'Lugar Creado de manera satisfactoria. El Mismo sera Pre-Evaluado y Se Le Informara Via Email. Cuando su Publicación sea Generada.');
            Session::flash('class', 'success');
            
        }
        else{
            Session::flash('mensaje', 'Hubó un error, por favor, verifica la información.');
            Session::flash('class', 'danger');
            return redirect()->intended(url('/gestion_espacios/create'))->withInput();
        }

        $files = $request->file('poplet');       
        
       
         foreach ($files as $file) {
          $poplet = new Galeria();
          
          // if ($request->hasFile($file)) {         
           $fileName =uniqid().'-'.$file->getClientOriginalName();
           $path = base_path('uploads/gestions/poplets/'. $gestion->id.'/');
           if ($file->getClientOriginalExtension()=="jpg" || $file->getClientOriginalExtension()=="png") {
                 $file->move($path, $fileName);
           
                    $poplet->imagen = $fileName;
                    $poplet->gestion_id = $gestion->id;
                    $poplet->save();


            }else{
                  Session::flash('mensaje', 'Hubo un error al guardar la galería.');
                   Session::flash('class', 'danger');
                  return redirect()->intended(url('/gestion_espacios/'.$gestion->id.'/edit'))->withInput();
                }

          //  }else{
           //   Session::flash('mensaje', 'Hubo un error al guardar la galería.');
           //   Session::flash('class', 'danger');
           // }


          } //end foreach
        


       

       return redirect()->intended(url('/gestion_espacios'))->withInput();
        



    
  }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GestionEspacios  $gestionEspacios
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lugar = GestionEspacios::find($id);

        if(count($lugar)<=0){

         return redirect()->intended(url('/gestion_espacios'))->withInput();

        }else{
      //  $oldpoplets=Galeria::where('gestion_id','=',$lugar->id)->get();
        $serv= explode(",", $lugar->servicios);//convertimos en un array los servicios
        $servEx=explode(",", $lugar->serviciosextra);
        $servicios = Servicio::find($serv);//buscamos registros coincidentes
        $serviciosextra = ServicioExtra::find($servEx);

        return view('Gestion_Espacios.show',compact('lugar','zonas','servicios','serviciosextra'));
    }






         
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GestionEspacios  $gestionEspacios
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
         $lugar = GestionEspacios::find($id);

        if(count($lugar)<=0){

         return redirect()->intended(url('/gestion_espacios'))->withInput();

        }else{
        $oldpoplets=Galeria::where('gestion_id','=',$lugar->id)->get();
        $zonas=Zona::orderBy('nombre','asc')->where('habilitado',true)->get();
        $servicios = Servicio::where('habilitado',true)->get();
        $serviciosextra = ServicioExtra::get();
        $serv= explode(",", $lugar->servicios);//convertimos en un array los servicios
        $servEx=explode(",", $lugar->serviciosextra);
        $servch = Servicio::find($serv);//buscamos registros coincidentes
        $servE = ServicioExtra::find($servEx);
       

        return view('Gestion_Espacios.edit',compact('lugar','oldpoplets','zonas','servicios','serviciosextra','servch','servE'));

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GestionEspacios  $gestionEspacios
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $lugar= GestionEspacios::find($id);

        $request->validate([
            'titulo' => 'required',           
            'descripcion' => 'required',
            'reglamento' => 'required',
            'tipo' => 'required',
            'zona'=>'required',
            'capacidad'=>'required|regex:/^[0-9 ,.\'-]+$/i',
            'latitud'=>'required|regex:/^[0-9 ,.\'-]+$/i',
            'longitud'=>'required|regex:/^[0-9 ,.\'-]+$/i',
            'telefono'=>'required|min:8|max:12|regex:/^[0-9 ,.\'-]+$/i',
            'tamaño'=>'required|regex:/^[0-9 ,.\'-]+$/i',
        ]);

         if (isset($request->servicio)) {
          $lugar->servicios=implode(",", $request->servicio);
        }
        else{
            Session::flash('mensaje', 'Selecciona o crea por lo menos un servicio.');
            Session::flash('class', 'danger');
           // return redirect()->intended(url('/gestions/nuevo'))->withInput();
            return redirect()->intended(url('/gestion_espacios/'.$id.'/edit'))->withInput();
        }


        //serviciosextra
        if (isset($request->serviciosextra)) {
          $lugar->serviciosextra=implode(",", $request->serviciosextra);
        }
        else{
            $lugar->serviciosextra=null;
        }


      //update iamgen destacada

        if (!empty($request->file('imagen'))){
          $img_ant=$lugar->imagen;//nombre de imagen anterior 
          $file = $request->file('imagen');
          if ($file->getClientOriginalExtension()=="jpg" || $file->getClientOriginalExtension()=="png") {
            $name = str_slug($request->name, '-') . "-". time()."." . $file->getClientOriginalExtension();
            $path = base_path('uploads/gestions/');
            $file-> move($path, $name);
            $lugar->imagen = $name;
            File::delete($path. $img_ant);//elimino imagen anterior
            

            }else{

            Session::flash('mensaje', 'El archivo no es una imagen valida.');
            Session::flash('class', 'danger');
           return back()->withInput();
          }

        }
      


        $lugar->Titulo=$request->titulo;
        $lugar->capacidad=$request->capacidad;
        $lugar->zona_id=$request->zona;
        $lugar->descripcion=$request->descripcion;
        $lugar->tamaño=$request->tamaño;
        $lugar->reglamento=$request->reglamento;
        $lugar->direccion=$request->direccion;
        $lugar->tamaño=$request->tamaño;
        $lugar->telefono_lugar=$request->telefono;
        $lugar->latitud=$request->latitud;
        $lugar->longitud=$request->longitud;
        $lugar->tipo=$request->tipo;

        if($lugar->save()){
              Session::flash('mensaje', 'La Información del Lugar Ha Sido Modificada de Manera Satisfactoria');
            Session::flash('class', 'success');        
           

        }else{

             Session::flash('mensaje', 'Modificación no ejecutada');
            Session::flash('class', 'danger');
           

        }

        ////////////GALERIA DE IMAGENES/////////////
        $files = $request->file('poplet');       
        

         if(count($request->poplet)>0){       
         foreach ($files as $file) {
          $poplet = new Galeria();
          //validoformato
          if ($file->getClientOriginalExtension()=="jpg" || $file->getClientOriginalExtension()=="png") {
          
      
           $fileName =uniqid().'-'.$file->getClientOriginalName();
           $path = base_path('uploads/gestions/poplets/'. $id.'/');       

                   $file->move($path, $fileName);
           
                    $poplet->imagen = $fileName;
                    $poplet->gestion_id = $id;
                    $poplet->save();
          }

          } 
        }

        ///////////////////END GALERY////////////////////////////////

        /// return redirect()->intended(url('/gestion_espacios/'.$id.'/edit'))->withInput();
         return back()->withInput();

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GestionEspacios  $gestionEspacios
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $lugar= GestionEspacios::find($id);
           $dir = base_path('uploads/gestions/');
          $path = base_path('uploads/gestions/poplets/'.$lugar->id.'/');
             File::delete($dir . $lugar->imagen);
            $oldpoplets=Galeria::where('gestion_id','=',$lugar->id)->get();
      
            foreach ($oldpoplets as $oldpoplet) {
                File::delete($path . $oldpoplet->imagen);
                $oldpoplet->delete();

            
            }
          $lugar->habilitado=false;
          $lugar->save();
            Session::flash('mensaje', 'El Lugar '.$lugar->Titulo.' Ha Sido eliminado con éxito.');
            Session::flash('class', 'success');
            return redirect()->intended(url('/gestion_espacios/'))->withInput();
    }



public function destroyimg($id){
    $galery=Galeria::find($id);
    if(count($galery)>0){
    $path = base_path('uploads/gestions/poplets/'.$galery->id.'/');
     File::delete($path .$galery->imagen);
     $galery->delete();

    Session::flash('mensaje', 'La Imágen Ha sido Eliminada de manera Satisfactoria');
    Session::flash('class', 'success');

    return back()->withInput();
   }else{
    return back()->withInput();

   }

      
           




}

    public function guardar($Modelo, Request $request)
    {   
        $req = $request->all();//<<<< Sacar las variables del request
        //dd($req);
        if (array_key_exists('_method',$req)) {
            unset($req['_method']);//<<< extraer la variable _token por 
        }if (array_key_exists('_token',$req)) {
            unset($req['_token']);//<<< extraer la variable _token por 
        }if (array_key_exists('confirm_password',$req)) {
            unset($req['confirm_password']);//<<< extraer la variable _token por 
        }if (array_key_exists('password',$req)) {
            $req['password'] = bcrypt($req['password']);//<<< extraer la variable _token por 
        }
        if(array_key_exists('picture',$req)) {
            $random = str_random(6);
            $destination = 'uploads/multimedios/';
            $image = $request->file('picture');
            $random = str_random(6);
            $filename = $random.$image->getClientOriginalName();
            $image->move($destination, $filename);
            $req['picture'] = $filename;
            
            //$this->salvar($Modelo,$req);
            foreach ($req as $key => $value) {// recorrer el request nuevo
            $Modelo->$key = $value;
                }
                if ($Modelo->save()) { // si guarda retorna true
                    return $Modelo;
                }    
        }else{
            foreach ($req as $key => $value) {// recorrer el request nuevo
                    $Modelo->$key = $value;
                }
                if ($Modelo->save()) { // si guarda retorna true
                    return $Modelo;
                }    
        }

            
    }


//filtrar lugares
     public function buscar(Request $request){

        if(empty($request->cuando)){

            return redirect()->intended(url('/'))->withInput();
        }
        if (isset($request->serv)) {
          $serv=implode(",",$request->serv);
          $serv1=explode(",",$serv);
       

          $lugares=Horario::tipo($request->get('tipo'))->zona($request->get('zona'))->capacidad($request->get('cuanto'))->precio($request->get('precio'))->fecha($request->get('cuando'))->whereHas('venue', function($q) use ($serv1){           
            $q->where('servicios',$serv1);    
         })->orderBy('nombre','asc')->get();

        }else{

          $lugares=Horario::tipo($request->get('tipo'))->zona($request->get('zona'))->capacidad($request->get('cuanto'))->precio($request->get('precio'))->fecha($request->get('cuando'))->orderBy('nombre','asc')->get();

        }
     

        $horarios=Horario::all();
        $servicios = Servicio::where('habilitado',true)->get();
        $zonas=Zona::orderBy('nombre','asc')->where('habilitado',true)->get();

        $cmin=$lugares->min('capacidad');
        $cmax=$lugares->max('capacidad');
        $min=$horarios->min('precio');
        $max=$horarios->max('precio');
      


      
        return view('lugares',compact('lugares','servicios','zonas','cmin','cmax','min','max'));
    }


     public function reservar(Request $request){

        $horario=Horario::where('venue_id',$request->venue)->where('fecha',$request->fecha)->first();
        
        if ($horario) {
            return view('reservar-lugar', ['horario'=>$horario]);
        }
        else{
            //return redirect()->intended(url('/404'));

         Session::flash('mensaje', 'La fecha  A Reservar No Se Encuentra Disponible');
         Session::flash('class', 'danger');
          return back()->withInput();
        }
    }



    public function vistalugar($id){

         $lugar = GestionEspacios::find($id);

        if(count($lugar)<=0){

         return redirect()->intended(url('/reservaClient'))->withInput();

        }else{
      //  $oldpoplets=Galeria::where('gestion_id','=',$lugar->id)->get();
        $serv= explode(",", $lugar->servicios);//convertimos en un array los servicios
        $servEx=explode(",", $lugar->serviciosextra);
        $servicios = Servicio::find($serv);//buscamos registros coincidentes
        $serviciosextra = ServicioExtra::find($servEx);

        return view('cliente.vistalugar',compact('lugar','zonas','servicios','serviciosextra'));

    }
  }


  public function destacar($id){

    $lugar = GestionEspacios::find($id);

    if($lugar->destacado==true){
      $mensaje="Ha Dejado de Estar Destacado";
      $lugar->destacado=false;

    }else{
      $lugar->destacado=true;
      $mensaje="Ha sido Destacado";
    }
      $lugar->save();

      Session::flash('mensaje', 'El Lugar ' .$lugar->Titulo .' ' .$mensaje);
    Session::flash('class', 'success');

    return redirect()->intended(url('/gestion_espacios'))->withInput();

  }

    public function showProv($id){

             $lugar = GestionEspacios::find($id);

        if(count($lugar)<=0){

         return back();

        }else{
      //  $oldpoplets=Galeria::where('gestion_id','=',$lugar->id)->get();
        $serv= explode(",", $lugar->servicios);//convertimos en un array los servicios
        $servEx=explode(",", $lugar->serviciosextra);
        $servicios = Servicio::find($serv);//buscamos registros coincidentes
        $serviciosextra = ServicioExtra::find($servEx);

        return view('admin.lugarFecha',compact('lugar','zonas','servicios','serviciosextra'));

    }
  }

}
