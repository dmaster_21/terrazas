<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Exports\UsersExport;
use App\Exports\ProveedorsExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Mail\Mailable;
use App\Mail\PeticionCall;
use Mail;

class UsuarioController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
      
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function crm_usuarios()
    {
        $usuarios= User::where('is_admin',0)->where('role','usuario')->where('status','Activo')->orderBy('name','asc')->get();
        return view('admin.usuarios', ['usuarios'=>$usuarios]);
    }

    public function crm_proveedores()
    {
        $usuarios= User::where('is_admin',0)->where('role','proveedor')->where('status','Activo')->orderBy('name','asc')->get();
        return view('admin.proveedores', ['usuarios'=>$usuarios]);
    }

    public function crm_espera()
    {
        $usuarios= User::where('is_admin',0)->where('status','Pendiente')->orderBy('name','asc')->get();
        return view('admin.usuariosEspera', ['usuarios'=>$usuarios]);
    }

    public function update(Request $request, $id)
    {
         $request->validate([
            'nombre' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|',            
            'empresa' => 'required|string', 
            'telefono' => 'required|string']);    
        
       
        $usr=User::find($id);
        $usr->name=$request->nombre;
        $usr->email=$request->email;
        $usr->empresa=$request->empresa;
        $usr->dob=$request->fecha;
        $usr->tel=$request->telefono;

        if($usr->save()){
         Session::flash('mensaje', 'Se Ha Modificado La Información de Manera Exitosa.!');
            Session::flash('class', 'success');           

        }else{
             Session::flash('mensaje', 'Ha Ocurrido Un Error Comuniquese con el administrador del Sistema.!');
            Session::flash('class', 'danger');   
        }
        
        //return redirect()->intended(url('/perfil'));
        return back()->withInput();

    }


     public function reset(Request $request, $id){



     $request->validate([
            'password' => 'required|string|min:6|confirmed']);

          $usr=User::find($id);
         $usr->password= bcrypt($request->password);

         if($usr->save()){
         Session::flash('mensaje', 'La Contraseña ha Sido  Modificada de Manera Exitosa.!');
            Session::flash('class', 'success');           

        }else{
             Session::flash('mensaje', 'Ha Ocurrido Un Error Comuniquese con el administrador del Sistema.!');
            Session::flash('class', 'danger');   
        }
        //return redirect()->intended(url('/perfil'));
        return back()->withInput();

     }

     public function Veruser($id){

      $usuarios=User::find($id);

      return view('admin.usuariosShow',compact('usuarios'));

    }

    

    public function inactivar($id,$status){
        $usuarios=User::find($id);
        $usuarios->habilitado=$status;

        if($status=='1'){
            $men="Habilitado";
           $usuarios->status='activo';
        }else{
            $men="Inhabilitado";
            $usuarios->status='Pendiente';

            
        }

       if($usuarios->save()){
    Session::flash('mensaje', 'El Usuario ha Sido '.$men .' De Manera Exitosa.!');
    Session::flash('class', 'success');           

        }else{
             Session::flash('mensaje', 'Ha Ocurrido Un Error Comuniquese con el administrador del Sistema.!');
            Session::flash('class', 'danger');   
        }

         return back()->withInput();


    }


    public function editprov(){

        $usuarios=User::find(auth()->id());

      return view('admin.perfilprov',compact('usuarios'));
    }

   public function excelUser(){
        
   return Excel::download(new UsersExport, 'crm_users.xlsx');
   
   }

   public function excelProv(){
    return Excel::download(new ProveedorsExport, 'crm_Prov.xlsx');
   }
      


}
