<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Openpay;
use App\Horario;
use App\Reservacion;
use App\User;
class OpenpayController extends Controller
{
 
	public function __construct()
	{
		$this->id = 'm5sl0mihifrfpcmus2gp';
		$this->secret = 'sk_bafff31b26e444309ef84afa6071f765';
		$this->middleware('auth');
	}

	public function index(Request $request)
	{


		try {

			Openpay::setSandboxMode(true);
			$openpay = Openpay::getInstance($this->id, $this->secret);


			$cliente = array(
				'name' => $request->get('nombre'),
				'last_name' =>'',
				'phone_number' => $request->get('telefono'),
				'email' => $request->get('correo'),
			);
			$pago = (float)preg_replace('/[^0-9]+/', '',$request->get('amount')) ;
			$datos = array(
				'method' => 'card',
				'source_id' => $request->get('token_id'),
				'amount' => $pago,
				'description' => 'Renta de Lugar Via Plataforma Tu Lugar (Online)',
				'device_session_id' => $request->get('deviceIdHiddenFieldName'),
				'customer' => $cliente,
			);
			


			if ($openpay->charges->create($datos)) {

	   //si logramos establecer el pago  verificamos si es renta o pago aparatado

	   if($request->forma=='apartado'){

	   	/*aqui ya esta creado el registro*/
	   	$reserva=Reservacion::where('tokens',$request->id_reserva)->where('user_id',auth()->id())->where('habilitado',1)->get();
//* verificamos que alquien no procesara el pago**//
	   	  foreach ($reserva as $resultado) {
        $info=Reservacion::where('horario_id',$resultado->horario_id)->where('status','rentado')->where('condicion',4)->get();

       if(count($info)>0){
         Session::flash('mensaje', 'Estimado Usuario te Informamos que el Lugar Ya Se Encuentra Rentado Te Invitamos A Ver Fechas Disponibles');
         Session::flash('class', 'danger');
          return redirect()->intended(url('/reservaClient'));
      }
    }

	   	foreach($reserva as $item){
        $user=User::find(auth()->id());
        $horario=Horario::find($item->horario_id);
        $horario->status='rentado';
        $horario->apartador=$user->name;
        $horario->save();


        $reserv=Reservacion::find($item->id);        
        $reserv->formapago='Openpay';
        $reserv->fechaPago=now();    
        $reserv->status='rentado';
        $reserv->condpago=true;
        $reserv->condicion=4;//renta procesada exitosamente
        $reserv->save();
        //listo
        }
   

	   }

	   if($request->forma=='rentado'){
	   $cart=\Session::get('cart');
	   	 $rand_part = str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789".uniqid());
        foreach($cart as $item){
        $user=User::find(auth()->id());

        $horario=Horario::find($item->id);
        $horario->status='rentado';
        $horario->apartador=$user->name;
        $horario->save();


        $reserv=new Reservacion();
        $reserv->venue_id=$item->venue_id;
        $reserv->horario_id=$item->id;
        $reserv->user_id=auth()->id();
        $reserv->fecha_reservada=$item->fecha;
        $reserv->formapago='Openpay';
        $reserv->fechaPago=now();    
        $reserv->status='rentado';      
        $reserv->costo_reserva=$item->precio;
        $reserv->condpago=true;
        $reserv->tokens=$rand_part;
        $reserv->condicion=4;//renta procesada exitosamente
        $reserv->save();
        }

			//$this->saveOrder(\Session::get('cart'));
			\Session::forget('cart');

		}
			return \Redirect::route('reserva.client')->with('mensaje','El Lugar Ha sido Rentado de Manera Satisfactoria.')->with('class', 'success');
			
				
		}else {
				return \Redirect::route('reserva.client')->with('mensaje','Hubó un error, por favor, Intenta Más Tarde.')->with('class', 'danger');
			
			}
		
			
		} catch (OpenpayApiTransactionError $e) {
			error_log('ERROR on the transaction: ' . $e->getMessage() . 
				' [error code: ' . $e->getErrorCode() . 
				', error category: ' . $e->getCategory() . 
				', HTTP code: '. $e->getHttpCode() . 
				', request ID: ' . $e->getRequestId() . ']', 0);

		} catch (OpenpayApiRequestError $e) {
			error_log('ERROR on the request: ' . $e->getMessage(), 0);

		} catch (OpenpayApiConnectionError $e) {
			error_log('ERROR while connecting to the API: ' . $e->getMessage(), 0);

		} catch (OpenpayApiAuthError $e) {
			error_log('ERROR on the authentication: ' . $e->getMessage(), 0);

		} catch (OpenpayApiError $e) {
			error_log('ERROR on the API: ' . $e->getMessage(), 0);

		} catch (Exception $e) {
			error_log('Error on the script: ' . $e->getMessage(), 0);
		}
	}



}
