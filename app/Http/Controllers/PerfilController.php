<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class PerfilController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */


    public function img_perfil(Request $request)
    {
        $usuarios= User::where('id',Auth::User()->id)->first();
        if(!empty($usuarios)){
            $destination = 'img/';
            $image = $request->file('img_perfil');
            $random = str_random(6);
            if (!empty($image)) {
                $filename = "/img/".$random.$image->getClientOriginalName();
                $image->move($destination, $filename);
                $usuarios->avatar = $filename;
            }
            $usuarios->save();

        }

        return redirect()->route('Miperfil');
    }


}
