<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class LiberaApartado extends Mailable
{
    use Queueable, SerializesModels;

      public $reserva;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($reserva)
    {
        //
        $this->reserva=$reserva;

    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
       
            return $this->markdown('emails.LiberaApartado')
                ->from('example@example.com')
                ->subject('TuLugar')->with('reserva',$this->reserva);
    }
}
