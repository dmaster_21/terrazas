<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PeticionCall extends Mailable
{
    use Queueable, SerializesModels;

  public $user;

    public function __construct($user)
    {
        $this->user = $user;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
       // return $this->from('example@example.com')
           //     ->view('');

                return $this->markdown('emails.peticion')
                ->from('example@example.com')
                ->subject('TuLugar | Aprobación del Lugar')->with('user',$this->user);
    
    }
}
