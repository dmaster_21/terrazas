<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Galeria extends Model
{
    protected $table = 'galeria';
	protected $fillable = ['gestion_id', 'imagen'];

	public function venue(){
	    return $this->belongsTo('App\Venue');
	}
}
