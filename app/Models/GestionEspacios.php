<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GestionEspacios extends Model
{
    protected $table = "gestion_espacios";

     public function zona(){
        return $this->belongsTo('App\Zona','zona_id');
    }
    public function usuario(){
        return $this->belongsTo('App\User','user_id');
    }

    public function galeria(){
	    return $this->hasMany('App\Galeria','gestion_id');
	}
	public function user(){
	    return $this->belongsTo('App\User','user_id');
	}

      public function horarios(){
        return $this->hasMany('App\Horario','venue_id','id');
    }

       public function reserva(){
        return $this->hasMany('App\Reservacion','venue_id','id');
    }
    

}
