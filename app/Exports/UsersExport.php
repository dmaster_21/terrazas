<?php

namespace App\Exports;

use App\User;
use App\Reservacion;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UsersExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function headings(): array
    {
        return [
            'Id',
            'Nombre',
            'Email',
            'Fecha de Nacimiento',
            'Genero',
            'Teléfono',
            'Empresa',
            'Lugares Rentados',
            
        ];
    }
    public function collection()
    {
         $users = User::where('role','usuario')->where('habilitado','1')->get();


         $data = collect();
          $data1="";

    foreach ($users as $key => $value) {

        $res=Reservacion::where('user_id',$value->id)->get();

        foreach($res as $ress){
           $data1= $ress->venue->Titulo.',';
        }

      $data->push([       
            
            'id' => $value->id,
            'nombre' => $value->name,
            'email'=> $value->email ,
            'Fecha de Nacimiento'=> $value->dob,
            'Genero' => $value->genero,
            'Teléfono' =>$value->tel,
            'Empresa'=>$value->empresa,
            'Lugares Rentados'=>$data1 

      ]);
    }
    return $data;
        
        
    }
}