<?php

namespace App\Exports;

use App\User;
use App\Reservacion;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ProveedorsExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function headings(): array
    {
        return [
            'Id',
            'Nombre',
            'Email',            
            'Teléfono',
            'Empresa',
            'Lugares',
            'Fechas Rentadas'
            
        ];
    }
    public function collection()
    {
         $users = User::where('role','proveedor')->where('habilitado','1')->get();


         $data = collect();
          $data1="";

    foreach ($users as $key => $value) {

        foreach ($value->venues as $key => $lugares) {
          $data1=$lugares->Titulo.',';
        }
       
      $data->push([       
            
            'id' => $value->id,
            'nombre' => $value->name,
            'email'=> $value->email ,           
            'Teléfono' =>$value->tel,
            'Empresa'=>$value->empresa,
            'Lugares '=>$data1 ,
            'Fechas Rentadas'=>''

      ]);
    }
    return $data;
        
        
    }
}