
@extends('templates.default')


@section('header')

@endsection
@section('pagecontent')
<section class="jumbotron text-center">
    <div class="container">
        <h1 class="jumbotron-heading">RESERVACIÓN</h1>
     </div>
</section>

<div class="container mb-4">
    <div class="row">
     
     @include('snip.notificaciones')

          @if($cart)
        <form action="{{action('ReservasController@store')}}" method="get">
            {!!csrf_field()!!}
           
            <div class="col-12">
                <center>
                <a href="{{url('/cart/trash')}}" class="btn btn-green"><i class="fa fa-trash"></i> Vaciar Reservación</a>
            </center>
            <hr>

            
                <div class="table-responsive">
                    
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Lugar</th>
                                    <th scope="col">Fecha</th>
                                    <th scope="col">Zona</th>
                                    <th scope="col">Tipo</th>
                                    <th scope="col">Capacidad</th>
                                    <th scope="col">Precio</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                              
                                 @foreach($cart as $item)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$item->nombre}}</td>
                                    <td>{{$item->fecha}}</td>
                                    <td>{{$item->venue->zona->nombre}}</td>
                                    <td>{{$item->tipo}}</td>
                                    <td>{{$item->capacidad}}</td>
                                    <td>${{number_format($item->precio,2,',','.')}} MXN</td>
                                    <td><a href="{{ route('cart.delete',$item->id)}}" class="aves-effect waves-green red btn"><i class="fa fa-trash"></a></td>
                                 
                                </tr>
                                @endforeach
                                
                                
                                

                                <tr>
                                    <td></td>

                                    <td><h5><strong>Total</strong></h5></td>
                                    <td colspan="5" class="text-right"> <h5><strong> ${{number_format($total,2,',','.')}}</span> MXN</strong></h5> </td>
                                </tr>

                                

                            </tbody>
                            <thead>
                                <tr>
                                    <th colspan="8"><center><h5><strong>Métodos de Pago</strong></th></h5>
                                </tr>
                                <tr>
                                    <td colspan="3" style="text-align: center;">
                               <div class="col s6">
                              <p>
                                  <input type="radio" name="metodo" value="Efectivo" id="mefectivo"  checked="" />
                                  <label for="mefectivo"><b>Efectivo</b> <i class="fa fa-dollar"></i></label>
                              </p>
                          </div>
                           
                         
                             </td>
                                    <td colspan="2" style="text-align: center;">
                                 <p>
                                  <input type="radio" name="metodo" value="Paypal" id="mpaypal"/>
                                  <label for="mpaypal"><i class="fa fa-paypal"></i>aypal</label>
                              </p>
                           
                                    </td>
                                    <td colspan="3" style="text-align: center;">
                                         <p>
                                  <input type="radio" name="metodo" value="OpenPay" id="mopen"/>
                                  <label for="mopen"><i class="fa fa-credit-card"></i> OpenPay</label>
                              </p>
                                    </td>
                                </tr>
                            </thead>
                        </table>

 
                    
                    
                </div>
            </div>
            <div class="">
                <div class="row">                  
                
                           
                   
                    <div class="col-xs-12  col-sm-4">
                        <a href="{{url('/')}}"  class="btn btn-lg  btn-success text-uppercase"><i class="fa fa-chevron-left"></i>Ver más lugares</a>
                    </div>
                     <div class="col-xs-12  col-sm-4">
                    </div>

                    <div class="col-xs-12  col-sm-4">
                        <button type="submit" class="btn btn-lg  btn-success text-uppercase"><i class="fa fa-check-circle"></i> Reservar</button>
                    </div>

                     
                </div>
            </div>
        </form>
        @else
        
        <div class="table-responsive">
                    
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Lugar</th>
                                    <th scope="col">Fecha</th>
                                    <th scope="col">Zona</th>
                                    <th scope="col">Tipo</th>
                                    <th scope="col">Capacidad</th>
                                    <th scope="col">Precio</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td colspan="7">
                                    <center>
                                    <h6>Estimado Usuario Usted Aún No Ha Seleccionado Un Lugar Para rentar!!</h6>
                                </center>
                                    
                                </td>
                            </tr>
                             <tr>
                                    <td></td>

                                    <td><h5><strong>Total</strong></h5></td>
                                    <td colspan="5" class="text-right"> <h5><strong> ${{number_format($total,2,',','.')}}</span> MXN</strong></h5> </td>
                                </tr>

                            </tbody>
                        </table>
                    
                     </div>    

                        <center>
                     <div class="col-md-4">
                        <a href="{{url('/')}}">
                        <button class="btn btn-lg  btn-success text-uppercase"><i class="fa fa-chevron-left"></i> Ver Más Lugares</button>
                    </a>
                   
                    </div>     
                        </center>


        @endif
    </div>
</div>
@endsection