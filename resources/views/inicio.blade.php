

@extends('templates.home')


@section('header')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

<style>
  .headerindex{
    display: block !important;
  }
  .headergrande{
  	background: transparent;
    height: 85px;
  }
  .nbsp{
  	display: none;
  }

</style>
@endsection
@section('pagecontent')

	<div class="main">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3 class="title-style-2">EXPLORA</h3>
				</div>
			</div>
			<div class="row">

			@if($venues)
  				@foreach($venues as $lugar)
				<div class="col s12 m4">
	
      <div class="card medium terraza-small">
        <div class="card-image">
          <img src="{{ url('uploads/gestions/'.$lugar->imagen) }}"  class="img-responsive" alt="">
         
          <a href="{{url('/lugar')}}/{{$lugar->id}}" class="btn-floating halfway-fab waves-effect waves-light btn btn-default top" title="Detalle del Lugar"><i class="fa fa-plus"></i></a>
        </div>
        <div class="card-content">
    <div class="card sticky-action">
    <div class="card-action">
    	<center>
    	<i class="fa fa-star"></i>
    	<i class="fa fa-star"></i>
    	<i class="fa fa-star"></i>
    	<i class="fa fa-star"></i>
    	<i class="fa fa-star-half"></i>
        </center>
    </div>

    <hr>

  </div>
      <span class="card-title activator grey-text text-darken-4">
      	<center>
      	<h6><strong>{{$lugar->Titulo}}</strong></h6>

      	<i class="fa fa-ellipsis-v  right"></i></span>
      </center>
    

      <p><a href="#"></a></p>
      <hr>


    </div>
    <div class="card-reveal">
      <span class="card-title grey-text text-darken-4"><h6>{{$lugar->Titulo}}</h6><i class="fa fa-times right"></i></span>
	<div class="table-responsive"></div>
      <table class="table-stirped">
      	<tr>
      		<td><i class="fa fa-map"></i> <b>Zona:</b>  {{$lugar->zona->nombre}}</td>
      	</tr>
      	<tr>
      		<td><i class="fa fa-users"></i> <b>Capacidad:</b>  {{$lugar->capacidad}}</td>
      	</tr>
      

      	<tr>
      		<td><i class="fa fa-home"></i> <b>Tipo: </b> {{$lugar->tipo}}</td>
      	</tr>
      	
      </table>
      
    </div>
     
	
   
    </div>
            
 </div>
		
  				@endforeach
			@endif
			</div>
			
		</div>
		<p>&nbsp;</p>
	</div>

	<div class="contact">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
				@include('snip.notificaciones')
			</div>
				<div class="col-md-12">

					<h3 class="title-style-2">CONTACTO</h3>
				</div>
			</div>
			<div class="row" style="margin-bottom: 0;">
				<div class="col-sm-8">
					<img src="{{ asset('/img/contact.jpg')}}" class="img-responsive wow bounceInRight" alt="">
				</div>
				<div class="col-sm-4">
					<div class="contact-box wow bounceInLeft">
						<form action="{{url('/enviar')}}" method="POST">
							 {!!csrf_field()!!}
							<h5>CONTÁCTANOS</h5>
		                    <div class="form-group">
		                      <input type="text" class="form-control browser-default" name="nombre" placeholder="Nombre" value="{{old('nombre')}}">
		                    </div>
		                    <div class="form-group">
		                      <input type="email" class="form-control browser-default" name="email" placeholder="Correo electrónico" value="{{old('email')}}">
		                    </div>
		                    <div class="form-group">
		                      <input type="tel" class="form-control browser-default" name="phone" placeholder="Teléfono" value="{{old('phone')}}">
		                    </div>
		                    <div class="form-group">
		                      <textarea class="form-control browser-default" name="mensaje" placeholder="Mensaje">{{old('mensaje')}}</textarea>
		                    </div>
		                    <div class="form-group">
		                      <label for="">&nbsp;</label>
		                      <button type="submit" class="btn btn-default">Enviar <i class="fa fa-send" style="font-size: 1rem;"></i></button>
		                    </div>
		                    
		                 </form>
					</div>
				</div>
			</div>
		</div>
		<p style="margin-bottom: 0;">&nbsp;</p>
	</div>


@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
    $('#zona').select2();
});
</script>
@endsection