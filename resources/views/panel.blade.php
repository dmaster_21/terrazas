@extends('templates.admin')

@section('pagecontent')
<div class=" main">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6">
        <h3 class="">Escritorio</h3>
      </div>
    </div>
<form action="{{url('prov')}}" method="post">
          {!! csrf_field() !!}
    <div class="row">
    

      <div class="col-sm-3 col-md-2">                
              <input type="text" id="desde" class="form-control datepicker browser-default" name="from" placeholder="Desde..." value="{{$from}}" readonly="">
          </div>
           <div class="col-sm-3 col-md-2">  
           <label for="desde" type="button" class="btn btn-yellow "><strong><i class="fa fa-calendar"></i></strong></label>              
              
          </div>
          <div class="col-sm-3 col-md-2">
              <input type="text" id="hasta" class="form-control datepicker browser-default" name="to" placeholder="Hasta..." value="{{$to}}" readonly="">
          </div>
          <div class="col-sm-3 col-md-2">                
              <label for="hasta" type="button" class="btn btn-yellow "><strong><i class="fa fa-calendar"></i></strong></label>
          </div>
          <div class="col-sm-3 col-md-2">
              <input type="submit" value="Ver periodo" class="btn btn-success">
          </div>

    
    </div>
    </form>
    <p>&nbsp;</p>

    

        
    <div class="row">
      
        <div class="col-md-6">
        <div class="card  darken-1">
                <div class="card-content">
                  <span class="card-title"></span>

                  <h5 style="font-weight: 700">Rentas de Lugares</h5>

                  <canvas id="polarChart"></canvas>


                </div>
                
            </div>
      </div>


       <div class="col-md-6">
        <div class="card  darken-1">
                <div class="card-content">
                  <span class="card-title"></span>

                  <h5 style="font-weight: 700">Lugares Vistos | Solicitados</h5>

                  <canvas id="horizontalBar"></canvas>


                </div>
                
            </div>
      </div>
      
      
    </div>

    


  </div>
</div>
@endsection

@section('scripts')
<script>
    //polar
  var ctxPA = document.getElementById("polarChart").getContext('2d');
  var myPolarChart = new Chart(ctxPA, {
    type: 'polarArea',
    data: {
      labels: ["Totales", "Pagadas", "Lista de Espera"],
      datasets: [{
        data: [{{$totales}},{{$rentadas}},{{$espera}}],
        backgroundColor: ["rgba(219, 0, 0, 0.1)", "rgba(0, 165, 2, 0.1)", "rgba(255, 195, 15, 0.2)",
          "rgba(55, 59, 66, 0.1)", "rgba(0, 0, 0, 0.3)"
        ],
        hoverBackgroundColor: ["rgba(219, 0, 0, 0.2)", "rgba(0, 165, 2, 0.2)",
          "rgba(255, 195, 15, 0.3)", "rgba(55, 59, 66, 0.1)", "rgba(0, 0, 0, 0.4)"
        ]
      }]
    },
    options: {
      responsive: true
    }
  });

 new Chart(document.getElementById("horizontalBar"), {
    "type": "horizontalBar",
    "data": {
      "labels": [
  
      @foreach($vistos as $visto)
      "{{$visto->Titulo}}",
      @endforeach
      ],
      "datasets": [{
        "label": "#Lugar Solicitado",
        "data": [
          @foreach($vistos as $visto)
      "{{$visto->qty}}",
      @endforeach
        ],
        "fill": false,
        "backgroundColor": ["rgba(255, 99, 132, 0.2)", "rgba(255, 159, 64, 0.2)",
          "rgba(255, 205, 86, 0.2)", "rgba(75, 192, 192, 0.2)", "rgba(54, 162, 235, 0.2)",
          "rgba(153, 102, 255, 0.2)", "rgba(201, 203, 207, 0.2)"
        ],
        "borderColor": ["rgb(255, 99, 132)", "rgb(255, 159, 64)", "rgb(255, 205, 86)",
          "rgb(75, 192, 192)", "rgb(54, 162, 235)", "rgb(153, 102, 255)", "rgb(201, 203, 207)"
        ],
        "borderWidth": 1
      }]
    },
    "options": {
      "scales": {
        "xAxes": [{
          "ticks": {
            "beginAtZero": true
          }
        }]
      }
    }
  });
</script>
@endsection