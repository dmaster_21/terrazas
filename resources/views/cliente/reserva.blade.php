@extends('templates.default')
@section('header')
<link rel="stylesheet" href="{{ url('js/data-tables/DT_bootstrap.css') }}" />
@endsection

@section('pagecontent')

<div class="container bootstrap snippet">
    <div class="row">
  		<div class="col-sm-10"><h1>{{Auth::user()->name}}</h1></div>
        <div class="col-sm-2">
            <form class="form-horizontal" role="form" method="POST" id="foto"  action="{{route('Miperfil-update-image')}}"  enctype="multipart/form-data">
                {!! csrf_field() !!}
            <label for="img_perfil"><img title="profile image" class="img-circle img-responsive" src="{{url(Auth::user()->avatar)}}">
                <input style="display: none;" type="file" name="img_perfil" id="img_perfil"> </label>
            </form>
        </div>
    	<!--<a href="#" class="pull-right"><img title="profile image" class="img-circle img-responsive" src="http://www.gravatar.com/avatar/28fd20ccec6865e2d5f0e1f4446eb7bf?s=100"></a></div>-->

    </div>
  
    <div class="row">
  	@include('templates.navCliente')
    	<div class="col-sm-9">
        <ul class="nav nav-tabs">
                <li ><a href="{{url('/perfil')}}"><i class="fa fa-user"></i> Mi Información</a></li>
               <li ><a data-toggle="tab" href="#lugares"><i class="fa fa-calendar"></i>Reservaciones</a></li>
              

                
              </ul>
                
            <div class="tab-content">
            <div class="tab-pane active" id="lugares">
                 @include('snip.notificaciones')
                <hr>


            <div class="table-responsive">
                    
                         <table class="display table table-bordered table-striped table-hover" id="dynamic-table">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Lugar</th>
                                    <th scope="col">Fecha</th>
                                    <th scope="col" class="text-center">Condición</th>
                                    <th scope="col">Fecha Solicitud</th>
                                    <th scope="col">Precio</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                              @if(count($reserva)>0)
                              
                                 @foreach($reserva as $item)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$item->venue->Titulo}}</td>
                                    <td>{{ date("Y/m/d", strtotime($item->fecha_reservada))}}</td>
                                    <td class="text-uppercase text-center">
                                       @if($item->status=='rentado')
                                        <span class="label label-success">{{$item->status}}</span>
                                        @else
                                        <span class="label label-warning">{{$item->status}}</span>
                                        @endif
                                        @if($item->condicion=='2')
                                        <span class="label label-danger">Lista Espera</span>    
                                        @endif
                                    </td>
                                    <td>{{$item->created_at}}</td>
                                    <td>${{number_format($item->costo_reserva,2,',','.')}} MXN</td>
                                    <td><a href="{{ route('vistalugarClient',$item->venue->id)}}" class="aves-effect waves-green  btn yellow btn-sm" title="Ver Detalle"><i class="fa fa-search"></i></a>
                                    @if($item->condicion==1)
                                    <a href="{{url('confirmaReserva/'.$item->tokens) }}" title="Procesar Pago Reserva" class="aves-effect waves-green  btn green"><i class="fa fa-cc-mastercard "></i></a>
                                    @endif


                                    </td>
                                 
                                </tr>
                            @endforeach
                            @else

                            @endif
                        </tbody>
                      </table>
                    </div>

           </div>

         </div>
       
        </div><!--/col-9-->
    </div><!--/row-->
</div>

@endsection
@section('scripts')
<script type="text/javascript" language="javascript" src="{{ url('js/advanced-datatable/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ url('js/data-tables/DT_bootstrap.js') }}"></script>
<!--dynamic table initialization -->
<script src="{{ url('js/dynamic_table_init.js') }}"></script>
<script>
    $(document).ready(function() {
        $('.table tr th:first-child').removeClass('sorting_desc');
        $('.table tr th:first-child').addClass('sorting');
        $('.table tr th:nth-child(3)').addClass('sorting_asc');
    });
    

        $('#img_perfil').change(function () {
            $('#foto').submit();
        });


</script>
@endsection