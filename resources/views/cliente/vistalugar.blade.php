@extends('templates.default')

@section('scripts')

    <script type="text/javascript">

        $('#img_perfil').change(function () {
            $('#foto').submit();
        });

    </script>
@endsection


@section('pagecontent')

<div class="container bootstrap snippet">
    <div class="row">
  		<div class="col-sm-10"><h1>{{Auth::user()->name}}</h1></div>
        <div class="col-sm-2">
            <form class="form-horizontal" role="form" method="POST" id="foto"  action="{{route('Miperfil-update-image')}}"  enctype="multipart/form-data">
                {!! csrf_field() !!}
            <label for="img_perfil"><img title="profile image" class="img-circle img-responsive" src="{{url(Auth::user()->avatar)}}">
                <input style="display: none;" type="file" name="img_perfil" id="img_perfil"> </label>
            </form>
        </div>
    	<!--<a href="#" class="pull-right"><img title="profile image" class="img-circle img-responsive" src="http://www.gravatar.com/avatar/28fd20ccec6865e2d5f0e1f4446eb7bf?s=100"></a></div>-->

    </div>
      <div class="row">
      <div class="col-md-12">
        @include('snip.notificaciones')
      </div>
    </div>

    <div class="row">
  	
    	<div class="col-sm-12">
        <ul class="nav nav-tabs">
                <li ><a href="{{url('/perfil')}}"><i class="fa fa-user"></i> Mi Información</a></li>
                <li><a  href="{{url('/reservaClient')}}"><i class="fa fa-calendar"></i> Reservaciones</a></li>
               <li class="active"><a data-toggle="tab" href="#milugar"><i class="fa fa-home"></i> Tu Lugar</a></li>
                
              </ul>

            <div class="tab-content">
            <div class="tab-pane active" id="milugar">
               <hr>

               <div class="col-md-8 card">
                

                    <h5><center>({{$lugar->Titulo}})</center></h5>

                        <div class="col s12">
                          <div class="row " >
                            <table class="table table-bordered">
                                <tr>
                                    <th>Nombre del Lugar:</th>
                                    <td>{{$lugar->Titulo}}</td>
                                    <th>Zona:</th>
                                    <td>{{$lugar->zona->nombre}}</td>
                                </tr>
                                <tr>
                                    <th>Capacidad:</th>
                                    <td>{{$lugar->capacidad}}</td>
                                    <th>Tipo:</th>
                                    <td>{{$lugar->tipo}}</td>
                                </tr>
                                <tr>
                                    <th colspan="4"><center>Descripción </center></th>          
                                </tr>
                                <tr>
                                  <td colspan="4">{{$lugar->descripcion}}</td>                                                              
                                </tr>

                                <tr>
                                    <th colspan="4"><center> Dirección </center></th>           
                                </tr>
                                <tr>
                                  <td colspan="4">{{$lugar->direccion}}</td>                                                                
                                </tr>

                                <tr>
                                    <th>Teléfono del Lugar:</th>
                                    <td>{{$lugar->telefono_lugar}}</td>
                                    <th>Tamaño:</th>
                                    <td>{{$lugar->tamaño}} Mts3</td>
                                </tr>
                                <tr>
                                    <th>Latitud:</th>
                                    <td>{{$lugar->latitud}}</td>
                                    <th>Longitud:</th>
                                    <td>{{$lugar->longitud}}</td>
                                </tr>

                                <tr>
                                    <th colspan="4"><center>Reglamento</center></th>
                                    
                                </tr>
                                <tr>
                                    <td colspan="4">{{$lugar->reglamento}}</td>
                                </tr>


                                
                            </table>



                          </div>

                         </div>
                            


                
            
            </div>

            <div class="col-md-4">



                    <h5>Servicios incluidos</h5>
                    <div>
                        @if($servicios)
                        @foreach($servicios as $servicio)
                          <div class="row">
                            <div class="col s12">
                              <p>
                                  <input type="checkbox"  checked="" disabled="" />
                                  <label for="cat{{$servicio->id}}">{{$servicio->nombre}}</label>
                              </p>
                            </div>
                          </div>
                        @endforeach
                        @endif
                    </div>

                    <h5>Imágen destacada</h5>
                    <div>
                          <div class="row">
                            <div class="col-md-12">
                            <img src="{{ url('uploads/gestions/'.$lugar->imagen) }}" class=" materialboxed" style="width: 100%;">

                            </div>
                           </div>
                    </div>
                    <hr>

                  


                
                    <h5 >Galería</h5>
                    <div class="popletsinput">
                        <center>
                        <a class="waves-effect waves-light btn modal-trigger" href="#galery"><i class="fa fa-eye"></i> Ver Galeria</a>
                    </center>
                    </div>
                    


                




            </div>
        </div>
        
    </div>
    

            </div>
        </div>
                
            
       
        </div><!--/col-9-->
    </div><!--/row-->
</div>
<!-- Modal Structure -->
      <div id="galery" class="modal">
        <div class="modal-content">
            <div class="modal-body">
          <h5>Galeria de ({{$lugar->Titulo}})</h5>
        
          <hr>

          <!--galery-->
         <div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    @foreach($lugar->galeria as $oldpoplet)
    <li data-target="#myCarousel" data-slide-to="{{$loop->iteration}}"></li>

    @endforeach
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      <img src="{{ url('uploads/gestions/'.$lugar->imagen) }}" alt="Imagen Destacada">
    </div>
    @foreach($lugar->galeria as $oldpoplet)
    <div class="item">

      <img src="{{ url('uploads/gestions/poplets/'.$oldpoplet->gestion_id.'/'.$oldpoplet->imagen) }}" alt="">
    </div>

    @endforeach
    
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<hr>
        <div class="modal-footer">
          <a href="#!" class="modal-action modal-close waves-effect waves-green btn" style="float: right;">Cancelar</a> 
            </div>
        </div>
        </div>


      </div>




</div>

@endsection