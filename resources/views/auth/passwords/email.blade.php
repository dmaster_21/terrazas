@extends('templates.default')

@section('pagecontent')
    <section class="entrar">
    <div class="container">
        <div class="row" style="margin: 0;">
        <div class="col-md-6 mb-4 col-md-offset-3">

            <div>
                <div class="card-body">
                    
                    <div class="row omb_row-sm-offset-3 social-login">
                        <div class="col-md-8 col-md-offset-2">
                           
                        </div>
                    </div>
                    <div class="section-title">
                        <b></b>
                        <span class="secition-title-main"></span>
                        <b></b>
                    </div>
                    
                    <h6 class="section-title-center py-3 text-center" > <span class="secition-title-main"><i class="fa fa-key"></i> Reestablecer Contraseña</span></h6>
                    @include('snip.notificaciones')
                    <!--Body-->
                     <form id="signupform" class="form-horizontal" role="form" action="{{ route('password.email') }}" method="post">

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="col-md-12">
                                	 <label for="email"><i class="fa fa-envelope-o grey-text fa-lg"></i> Email</label>
                                    <input type="email" name="email" id="email" value="{{ old('email') }}" class="form-control">
                                   
                                </div>
                       
                        <div>
                            <button class="btn btn-default waves-effect waves-light">Enviar Enlace de Recuperación</button>
                            &nbsp; &nbsp; &nbsp; &nbsp; 
                         
                              
                            
                        </div>
                        
                    </form>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <p>
                        
                        </p>
                        
                    </div>
                    <div class="col-sm-6">
                        <p>
                           
                        </p>
                    </div>
                </div>
            </div>
        </div>

        
    </div>
    </div>
</section>

@endsection
