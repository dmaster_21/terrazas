@extends('templates.default')


@section('header')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

@endsection
@section('pagecontent')

	<div class="main">
		<div class="container">
			<div class="row">
				<div class="col s12">
					<h3 class="title-style-2">EXPLORA</h3>
				</div>
				<hr>
			</div>
	<!--inicio de divicion-->
			<div class="row">
			<div class="col-md-12">
					<!--filtro-->
				<div class="col-md-4">

					<form action="{{action('GestionEspaciosController@buscar')}}" method="get">
			
				<div class="col-md-12">
				<button class="btn btn-default"><i class="fa fa-search"></i>Filtrar Lugar</button>
			</div>

					<!--open filtro precio-->
					<div class="col-md-12">
						<div class="sidebar card">	    
				
						<label for=""><strong>$ Precio</strong></label>
						<p class="range-field">
					      <input type="range" id="precio" name="precio" min="{{$min}}" max="{{$max}}" value="{{$min}}" />
					    </p>
						
					</div>						
					</div>
					<!--open tipo-->
					<div class="col-md-12">
						<div class="sidebar card">
						<label for=""><strong><i class="fa fa-star"></i>Tipo de Lugar</strong></label>
						<p>
     					 <input type="radio" name="tipo" value="Salón" class="filled-in" id="salon"  />
     					 <label for="salon">Salón</label>
    					</p>

    					<p>
     					 <input type="radio"  name="tipo" value="Jardín" class="filled-in" id="jardin"  />
     					 <label for="jardin">Jardín</label>
    					</p>

    					<p>
     					 <input type="radio"  name="tipo" value="Terraza" class="filled-in" id="terrazas"  />
     					 <label for="terrazas">Terrazas</label>
    					</p>					




					</div>
						
					</div>
					<!--end tipo-->
					
					<!--open fecha-->
					<div class="col-md-12">
						<div class="sidebar card">
						<label for="cuando"><strong><i class="fa fa-calendar"></i> Fecha</strong></label>
						 <input name="cuando" type="text" class="form-control browser-default datepicker" id="cuando" value="{{now()->format('Y-m-d')}}" readonly="">	

						 <div class="form-group text-center" >
                        <br>
                        <label for="cuando" type="button" class="btn btn-default" style="color: #fff;width: 45%"><i class="fa fa-calendar"></i></label>
                      </div>					
					</div>						
					</div>
					<!--end fecha-->


					<!--capacidad-->
					<div class="col-md-12">
					<div class="sidebar card">
						<label for=""><strong><i class="fa fa-users"></i> Capacidad</strong></label>
						<p class="range-field">
					      <input type="range" id="cuanto" name="cuanto" min="{{$cmin}}" max="{{$cmax}}" value="{{$cmin}}" />
					    </p>						
					</div>
					</div>

					<!--end capacidad-->


					<div class="col-md-12">

						<div class="sidebar card">
						<label for=""><strong><i class="fa fa-map-marker"></i>  Zonas</strong></label>
						<select id="zona" name="zona" class="js-example-basic-single form-control">	
						<option value="">-Seleccionar Zona-</option>						
					@foreach($zonas as $zona)
					<option value="{{$zona->nombre}}">{{$zona->nombre}}</option>
				       <!-- <p>

     					 <input type="radio" name="zona" value="{{$zona->nombre}}" class="filled-in" id="zona{{$zona->id}}"  />
     					 <label for="zona{{$zona->id}}">{{$zona->nombre}}</label>
     					</p>-->
    					
					@endforeach
						</select>				
					</div>
				</div>
					<!--end zonas-->

					<div class="col-md-12">
						<div class="sidebar card">
						<label for=""><strong><i class="fa fa-map-marker"></i>  Servicios</strong></label>
						@foreach($servicios as $servicio)
						<p>
     					 <input type="checkbox" name='serv[]' value="{{$servicio->id}}" class="filled-in" id="serv{{$servicio->nombre}}"  />
     					 <label for="serv{{$servicio->nombre}}">{{$servicio->nombre}}</label>
     					</p>
    					
					@endforeach						
					</div>
					</div>

					<!--end servicios-->
						
				
				</form>
				</div>
				<!--end casilla-->

				<!--resultado-->
				<div class="col-md-8">

					<div class="col-md-12">

	<!--aqui va todo-->
			  	@if($lugares)
				@foreach($lugares as $lugar)
		
   <div class="col s12 m6">
	
      <div class="card small terraza-small">
        <div class="card-image">
          <img src="{{ url('uploads/gestions/'.$lugar->venue->imagen) }}"  class="img-responsive" alt="">
         
          <a href="{{url('/lugar')}}/{{$lugar->venue_id}}" class="btn-floating halfway-fab waves-effect waves-light btn btn-default top" title="Detalle del Lugar"><i class="fa fa-plus"></i></a>
        </div>
        <div class="card-content">
    <div class="card sticky-action">
    <div class="card-action">
    	<center>
    	<i class="fa fa-star"></i>
    	<i class="fa fa-star"></i>
    	<i class="fa fa-star"></i>
    	<i class="fa fa-star"></i>
    	<i class="fa fa-star-half"></i>
        </center>
    </div>

    <hr>

  </div>
      <span class="card-title activator grey-text text-darken-4">
      	<center>
      	<h6><strong>{{$lugar->nombre}}</strong></h6>

      	<i class="fa fa-ellipsis-v  right"></i></span>
      </center>
    

      <p><a href="#"></a></p>
      <hr>


    </div>
    <div class="card-reveal">
      <span class="card-title grey-text text-darken-4"><h6>{{$lugar->nombre}}</h6><i class="fa fa-times right"></i></span>
	<div class="table-responsive"></div>
      <table class="table-stirped">
      	<tr>
      		<td><i class="fa fa-map"></i> <b>Zona:</b>  {{$lugar->venue->zona->nombre}}</td>
      	</tr>
      	<tr>
      		<td><i class="fa fa-users"></i> <b>Capacidad:</b>  {{$lugar->capacidad}}</td>
      	</tr>
      

      	<tr>
      		<td><i class="fa fa-home"></i> <b>Tipo: </b> {{$lugar->tipo}}</td>
      	</tr>
      	
      </table>
      
    </div>
     
	
   
    </div>
           
 </div>
							
			
			   @endforeach
			@endif

				@if(count($lugares)==0)

				<div class="col s12 text-center">
					<i class="fa fa-exclamation-circle fa-4x" aria-hidden="true"></i>

					<br>
					<h3 class="title-style-2">DiSCULPE NO ENCONTRAMOS RESULTADOS </h3>
					<hr>
				</div>
				@endif

						
					</div>



				</div>

			</div>
		  </div>

    <!--end divicion -->		  
		</div>
	</div>

@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
    $('.js-example-basic-single').select2();
});
</script>
@endsection