
@extends('templates.default')


@section('header')

@endsection
@section('pagecontent')
<section class="jumbotron text-center">
    <div class="container">
        <h1 class="jumbotron-heading">CONFIRMAR PAGO</h1>
     </div>
</section>

<div class="container mb-4">
    <div class="row">
     
     @include('snip.notificaciones')

       
        <form action="{{action('ReservasController@continuarPago')}}" method="post">
            {!!csrf_field()!!}
           <input type="hidden" name="tokens" value="{{$tokens}}">
            <div class="col-12">
               
            <hr>

            
                <div class="table-responsive">
                    
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Lugar</th>
                                    <th scope="col">Fecha</th>
                                    <th scope="col">Zona</th>
                                    <th scope="col">Tipo</th>
                                    <th scope="col">Capacidad</th>
                                    <th scope="col">Precio</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                           <?php $sum =0; ?>
                                 @foreach($reserva as $item)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$item->venue->Titulo}}</td>
                                    <td>{{$item->fecha_reservada}}</td>
                                    <td>{{$item->venue->zona->nombre}}</td>
                                    <td>{{$item->venue->tipo}}</td>
                                    <td>{{$item->venue->capacidad}}</td>
                                    <td>{{number_format($item->costo_reserva,2,',','.')}} MXN</td>
                                    <td></td>
                                 <?php $sum += $item->costo_reserva; ?>
                                </tr>
                                @endforeach
                                
                                
                                

                                <tr>
                                    <td></td>

                                    <td><h5><strong>Total</strong></h5></td>
                                    <td colspan="5" class="text-right"> <h5><strong> </span> {{number_format($sum,2,',','.')}} MXN</strong></h5> </td>
                                </tr>

                                

                            </tbody>
                            <thead>
                                <tr>
                                    <th colspan="8"><center><h5><strong>Métodos de Pago</strong></th></h5>
                                </tr>
                                <tr>
                                    <td colspan="3" style="text-align: center;">
                               <div class="col s6">
                              <p>
                                  <input type="radio" name="metodo" value="Efectivo" id="mefectivo"  checked="" />
                                  <label for="mefectivo"><b>Efectivo</b> <i class="fa fa-dollar"></i></label>
                              </p>
                          </div>
                           
                         
                             </td>
                                    <td colspan="2" style="text-align: center;">
                                 <p>
                                  <input type="radio" name="metodo" value="Paypal" id="mpaypal"/>
                                  <label for="mpaypal"><i class="fa fa-paypal"></i>aypal</label>
                              </p>
                           
                                    </td>
                                    <td colspan="3" style="text-align: center;">
                                         <p>
                                  <input type="radio" name="metodo" value="OpenPay" id="mopen"/>
                                  <label for="mopen"><i class="fa fa-credit-card"></i> OpenPay</label>
                              </p>
                                    </td>
                                </tr>
                            </thead>
                        </table>

 
                    
                    
                </div>
            </div>
            <div class="">
                <div class="row">                  
                
                           
                 

                    <div class="col-xs-12  col-sm-4">
                        <button type="submit" class="btn btn-lg  btn-success text-uppercase"><i class="fa fa-check-circle"></i> Procesar</button>
                    </div>

                     
                </div>
            </div>
        </form>
       
    </div>
</div>
@endsection