@extends('templates.default')


@section('header')

@endsection
@section('pagecontent')
<section class="jumbotron text-center">
    <div class="container">
        <h1 class="jumbotron-heading"><img src="{{ url('img/login_logo.png') }}"></h1>
     </div>
</section>
<div class="container">
     <div class="card  darken-1">
                      <div class="card-content">
                         
    <div class="row">
             <div class="col-md-12">
                

                            <form action="{{action('OpenpayController@index')}}" method="POST" id="payment-form">
                        <input type="hidden" name="token_id" id="token_id">
                        <input type="hidden" name="_token"value="{{ csrf_token()}}">
                        <input type="hidden" name="id_reserva" value='{{$tokens}}'>
                        <input type="hidden" name="forma" value="{{$forma}}">
                        <div class="col-md-6">
                        <h5><i class="fa fa-user"></i> Datos del Cliente</h5>
                        <div class="form-group">
                            <div class="row">

                                <div class="col-md-12">
                                    <label>Nombre del cliente</label>
                                    <input class="form-control" type="text" name="nombre" placeholder="Nombre del cliente" value="{{ Auth::user()->name }}" readonly="">
                                </div>
                              
                                <div class="col-md-6">
                                    <label>Correo</label>
                                    <input class="form-control" type="email" name="correo" value="{{ Auth::user()->email}}" readonly="">
                                </div>
                                <div class="col-md-6">
                                    <label>Telefono</label>
                                    <input class="form-control" type="text" name="telefono" value="{{ Auth::user()->tel}}" required>
                                </div>
                                <div class="col-md-12">
                                    <label>Ingresa el monto a cobrar</label>
                                    <input class="form-control" type="number" maxlength="4" name="amount" value="{{$costo}}" readonly="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h5><i class="fa fa-credit-card"></i> Datos de la tarjeta</h5>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Nombre del titular</label>
                                    <input class="form-control" type="text" placeholder="Como aparece en la tarjeta" autocomplete="off" data-openpay-card="holder_name">
                                </div>
                                <div class="col-md-6">
                                    <label>Número de tarjeta</label>
                                    <input class="form-control" type="text" autocomplete="off" maxlength="16" data-openpay-card="card_number" required>
                                </div>
                                <div class="col-md-12" style="margin-top: 20px">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>Mes</label>
                                            <input class="form-control" type="text" placeholder="Mes" data-openpay-card="expiration_month" maxlength="2"required>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Año</label>
                                            <input class="form-control" type="text" placeholder="Año" data-openpay-card="expiration_year" maxlength="2" required>
                                        </div>
                                        <div class="col-md-4">
                                            <label>CCV</label>
                                            <input class="form-control" type="text" placeholder="3 dígitos" autocomplete="off" data-openpay-card="cvv2" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                <div class="col-md-12" style="margin-top: 20px;color:white">
                                    <a id="pay-button" class="btn btn-primary"><i class="fa fa-check-circle"></i> Pagar ahora</a>
                                </div>
                            </div>
                        </div>
                   </div>
                    </form>

                     </div>
                 </div>
           
     </div>
 </div>
<script type="text/javascript" 
src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script type="text/javascript" 
src="https://openpay.s3.amazonaws.com/openpay.v1.min.js"></script>
<script type='text/javascript' 
src="https://openpay.s3.amazonaws.com/openpay-data.v1.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        OpenPay.setId('m5sl0mihifrfpcmus2gp');
        OpenPay.setApiKey('pk_c2fd38f51db04f34b06e5134eb8185ee');
        OpenPay.setSandboxMode(true);
            //Se genera el id de dispositivo
            var deviceSessionId = OpenPay.deviceData.setup("payment-form", "deviceIdHiddenFieldName");
            
            $('#pay-button').on('click', function(event) {
                event.preventDefault();
                $("#pay-button").attr("disabled", true);
                OpenPay.token.extractFormAndCreate('payment-form', sucess_callbak, error_callbak);                
            });

            var sucess_callbak = function(response) {
              var token_id = response.data.id;
              $('#token_id').val(token_id);
              $('#payment-form').submit();
          };

          var error_callbak = function(response) {
            var desc = response.data.description != undefined ? response.data.description : response.message;
            alert("ERROR [" + response.status + "] " + desc);
            $("#pay-button").attr("disabled", false);
            
        };

    });
</script>
</script>
@endsection