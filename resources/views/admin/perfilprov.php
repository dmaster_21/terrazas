@extends('templates.admin')
@section('header')
<link rel="stylesheet" href="{{ url('js/data-tables/DT_bootstrap.css') }}" />
@endsection
@section('pagecontent')
<div class=" main">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-6">
				<h3 class="">Usuarios</h3>
			</div>
			<div class="col-md-6 text-right valign-wrapper" style="justify-content: space-between;">
				<!--div class="text-center" style="margin-left: auto;">
					<a href="{{url('/usuarios/nuevo')}}" class="btn btn-primary right waves-effect waves-light btn-large">Añadir nuevo</a>
				</div-->
				
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				@include('snip.notificaciones')
			</div>
		</div>
	
		
		<div class="col-md-4">
      <div class="card">
        <div class="card-image">
          <img src="{{url($usuarios->avatar)}}">
          <span class="card-title"></span>
          <a class="btn-floating halfway-fab waves-effect waves-light modal-trigger" href="#clave"><i class="fa fa-key"></i></a>
        </div>
        <div class="card-content">
          <p><h6><b>{{$usuarios->name}}</b></h6></p>
          <hr class="text-center">

             <a href="{{url('/crm_usuarios')}}" class="modal-action modal-close waves-effect waves-green  btn text-center"><i class="fa fa-chevron-circle-left"></i>  Volver</a> 
        </div>
      </div>
    </div>

		  <div class="col-md-8">
        <div class="card  darken-1">
                <div class="card-content">
                  <span class="card-title">Infomacion Básica del Usuario</span>
                  <form action="{{action('UsuarioController@update',$usuarios->id)}}" method="GET" id="registrationForm" novalidate="">
                  	<input type="hidden" name="_token"value="{{ csrf_token()}}">

                  <div class="md-form input-field">
				
                  			<div class="md-form input-field">
                             <label for="nombre"><i class="fa fa-user-o grey-text fa-lg"></i> Nombre completo</label>
                            <input type="text" name="nombre" id="nombre" class="form-control" value="{{$usuarios->name}}" required>
                           
                        </div>

                            <label for="email"><i class="fa fa-envelope-o grey-text fa-lg"></i> Email</label>
                            <input type="email" name="email" id="email" class="form-control" value="{{$usuarios->email}}" required>
                            
                        </div>
                        <div class="md-form input-field">
                            <label for="empresa"><i class="fa fa-building grey-text fa-lg"></i> Empresa</label>
                            <input type="text" name="empresa" id="empresa" class="form-control" value="{{$usuarios->empresa}}">
                            
                        </div>
                        <div class="md-form input-field">
                            <label for="dob"><i class="fa fa-calendar grey-text fa-lg"></i> Fecha de nacimiento</label>
                            <input type="text" name="fecha" id="dob" value="{{$usuarios->dob}}" class="form-control datepicker" required>
                            
                        </div>
                        <div class="md-form input-field">
                              <label for="tel"><i class="fa fa-phone grey-text fa-lg"></i> Teléfono</label>
                            
                            <input type="text" name="telefono" id="tel" class="form-control" value="{{$usuarios->tel}}" required>
                          
                        </div>
                        <label for="defaultForm-email"><i class="fa fa-venus-mars grey-text fa-lg"></i> Genero</label>
                        <div class="md-form">
                            
                        <p><input name="genero" id="masculino" type="radio" value="Masculino" @if($usuarios->genero=='Masculino')checked="" @endif   required/><label for="masculino">Masculino</label>  &nbsp;   &nbsp;   &nbsp; 
                            <input name="genero" id="femenino" type="radio" value="Femenino" @if($usuarios->genero=='Femenino')checked="" @endif required/><label for="femenino">Femenino</label></p>
                           
                            
                        </div>
						<hr>
						<div class="text-center">
                            <button class="btn btn-default waves-effect waves-light"><i class="fa fa-edit"></i> Actualizar</button>
								
							@if($usuarios->habilitado==1)
                             <a class="btn btn-default waves-effect waves-light" href="{{ route('habilitar.user',['id'=> $usuarios->id,'status'=>'0']) }}"><i class="fa fa-check-circle"></i>  Inhabilitar </a>
                             @else

                             <a class="btn btn-default waves-effect waves-light" href="{{ route('habilitar.user',['id'=>$usuarios->id,'status'=>'1']) }}"><i class="fa fa-check-circle"></i> habilitar </a>
                             @endif
                        </div>


                    </form>
 
        



                </div>
                
            </div>
      </div>

		


	



 <div id="clave" class="modal">
      <div class="modal-content">
        <h4>Modificar Contraseña</h4>
        <p>¿Está seguro que desea Cambiar su contraseña ?</p>

       
      <form action="{{action('UsuarioController@reset',$usuarios->id)}}" method="POST" novalidate="">
	
         <div class="md-form input-field">
                             <label for="defaultForm-pass"><i class="fa fa-lock grey-text fa-lg"></i> Contraseña</label>
                            <input type="password" name="password" id="defaultForm-pass" class="form-control validate" required>
                           
                        </div>
                        <div class="md-form input-field">
                               <label for="defaultForm-pass-confirm"><i class="fa fa-lock grey-text fa-lg"></i> Confirmar contraseña</label>
                            <input type="password" name="password_confirmation" id="defaultForm-pass-confirm" class="form-control validate" required>
                         
                        </div>

        <input type="hidden" name="_token"value="{{ csrf_token()}}">


		<hr>
		<div class="text-center">
        <input type="submit" class="waves-effect waves-green btn" value="Cambiar Clave">

         <a href="#!" class="modal-action modal-close waves-effect waves-green red btn">Cancelar</a> 
        </div>

        

      
      </form>
      <p> &nbsp;</p>
      </div>


    </div>


@endsection

@section('scripts')
<script type="text/javascript" language="javascript" src="{{ url('js/advanced-datatable/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ url('js/data-tables/DT_bootstrap.js') }}"></script>
<!--dynamic table initialization -->
<script src="{{ url('js/dynamic_table_init.js') }}"></script>
<script>
	$(document).ready(function() {
		$('.table tr th:first-child').removeClass('sorting_desc');
		$('.table tr th:first-child').addClass('sorting');
		$('.table tr th:nth-child(3)').addClass('sorting_asc');
	});
	
</script>
@endsection