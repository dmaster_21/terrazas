@extends('templates.admin')

@section('pagecontent')
<div class=" main">
	<div class="container-fluid">
		<div class="row" >
			<div class="card">
    <div class="card-content">
    	<div class="row">
			<h3 class="text-center">Inspección de Reserva</h3>
		<hr>

		<div class="col-md-12">
				@include('snip.notificaciones')
				@if(count($detalle)==0)
				<div class="alert alert-danger alert-dismissable">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    Disculpe No se Encontraron resultados en El Parámetro de Busqueda
   
  </div>
				@endif
		</div>
		
			<form action="{{action('ReservasController@reservavista')}}" method="get" >
				
		<div class="col-md-6">
					<label for="venuen">Lugar</label>
					<br>
		          <select name="lugar" id="venuen" class="select">
		          	<option value="">Selecciona...</option>
		          	@foreach($lugar as $lugares)
					<option value="{{$lugares->id}}">{{$lugares->Titulo}}</option>
		          	@endforeach
		          	
		          </select>
		          
		        </div>
				<div class="col-md-4">
					<label for="fecha">Fecha</label>
		          <input id="fecha" name="fecha" type="text" class="validate datepicker" value="{{old('fecha')}}" required>
		          
		        </div>
		        <div class="col-md-2"><br>
					 <label for="fecha" type="button" class="btn btn-yellow "><strong><i class="fa fa-calendar"></i></strong></label>
		         
		          
		        </div>
		<div class="col-md-12 text-center"><button class="btn btn-default"><i class="fa fa-check-circle"></i> Procesar Solicitud</button></div>
		<hr>
     	</div>
     </form>
     	</div>
     </div>
 </div>
</div>

<hr>



@endsection

@section('scripts')
<script type="text/javascript" language="javascript" src="{{ url('js/advanced-datatable/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ url('js/data-tables/DT_bootstrap.js') }}"></script>
<!--dynamic table initialization -->
<script src="{{ url('js/dynamic_table_init.js') }}"></script>
<script>
	$('#info2').modal('show');
	$(document).ready(function() {
		$('.table tr th:first-child').removeClass('sorting_desc');
		$('.table tr th:first-child').addClass('sorting');
		$('.table tr th:nth-child(3)').addClass('sorting_asc');
	});
	
</script>


   
@endsection