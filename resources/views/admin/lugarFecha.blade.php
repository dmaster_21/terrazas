@extends('templates.admin')

@section('pagecontent')
<div class=" main">
	<div class="container-fluid">
		
		
		<div class="row">
			<div class="col-md-6">
				<h5 class="">Detalle Del Lugar</h5>
			</div>
			
		</div>
		<div class="row">
			
		</div>
		
		<div class="row">


			<div class="col-md-8 card">
				

				    <h5><center>({{$lugar->Titulo}})</center></h5>

				    	<div class="co-md-12">
					      <div class="row " >
					      	<table class="table table-bordered">
					      		<tr>
					      			<th>Nombre del Lugar:</th>
					      			<td>{{$lugar->Titulo}}</td>
					      			<th>Zona:</th>
					      			<td>{{$lugar->zona->nombre}}</td>
					      		</tr>
					      		<tr>
					      			<th>Capacidad:</th>
					      			<td>{{$lugar->capacidad}}</td>
					      			<th>Tipo:</th>
					      			<td>{{$lugar->tipo}}</td>
					      		</tr>
					      		<tr>
					      			<th colspan="4"><center>Descripción </center></th>			
					      		</tr>
					      		<tr>
					      		  <td colspan="4">{{$lugar->descripcion}}</td>				      							      			
					      		</tr>

					      		<tr>
					      			<th colspan="4"><center> Dirección </center></th>			
					      		</tr>
					      		<tr>
					      		  <td colspan="4">{{$lugar->direccion}}</td>				      							      			
					      		</tr>

					      		<tr>
					      			<th>Teléfono del Lugar:</th>
					      			<td>{{$lugar->telefono_lugar}}</td>
					      			<th>Tamaño:</th>
					      			<td>{{$lugar->tamaño}} Mts3</td>
					      		</tr>
					      		<tr>
					      			<th>Latitud:</th>
					      			<td>{{$lugar->latitud}}</td>
					      			<th>Longitud:</th>
					      			<td>{{$lugar->longitud}}</td>
					      		</tr>

					      		<tr>
					      			<th colspan="4"><center>Reglamento</center></th>
					      			
					      		</tr>
					      		<tr>
					      			<td colspan="4">{{$lugar->reglamento}}</td>
					      		</tr>


					      		
					      	</table>

					      	<hr>

					      	<div class="text-center">
					      		<a href="/verUser/{{$lugar->user_id}}"><button class="btn btn-default" ><i class="fa fa-chevron-circle-left"></i> Volver</button></a>
					      		
					      	</div>
					      	<hr>



					      </div>

					     </div>
					        


				
			
			</div>

			<div class="col-md-4">

				    <h5>Servicios incluidos</h5>
				    <div>
				    	@if($servicios)
				    	@foreach($servicios as $servicio)
					      <div class="row">
					        <div class="co-md-12">
					          <p>
							      <input type="checkbox"  checked="" disabled="" />
							      <label for="cat{{$servicio->id}}">{{$servicio->nombre}}</label>
						      </p>
					        </div>
					      </div>
					    @endforeach
					    @endif
				    </div>

				    


				
				    <h5>Imágen destacada</h5>
				    <div>
					      <div class="row">
					        <div class="col-md-12">
					        <img src="{{ url('uploads/gestions/'.$lugar->imagen) }}" class=" materialboxed" style="width: 60%;">

					        </div>
					       </div>
				    </div>
				    <hr>

				  


				
				    <h5 >Galería</h5>
				    <div class="popletsinput">
				    	<center>
				    	<a class="waves-effect waves-light btn modal-trigger" href="#galery"><i class="fa fa-eye"></i> Ver Galeria</a>
				    </center>
				    </div>
				    


				




			</div>
		</div>
		
	</div>


	<!--fechas que han sido rentadas-->
	<div class="col-md-12">
        <div class="card  darken-1">
                <div class="card-content">
                  <span class="card-title">Fechas rentadas del Lugar</span>
                </div>
                <div class="adv-table table-responsive">
					
				<div class="col-md-12">	
			  <table class="display table table-bordered table-striped table-hover" id="dynamic-table">
			  <thead>
			  	<tr>		      	
			      	
					<th class="sorting_desc">Fecha</th>
			      	<th>Usuario que Rentó</th>
			      
			  	</tr>
			  </thead>
			  <tbody>
			  	@foreach($lugar->horarios as $horario)
			  	
			  	<tr>
			      	
			      	
					<td class="sorting_desc">{{$horario->fecha}}</td>
			      	<td>{{$horario->apartador}}</td>
			      	
			  	</tr>
			  	
             @endforeach		



			  </tbody>
			  <tfoot>
			  	<tr>
			      <th class="sorting_desc">Fecha</th>
			      <th>Usuario que Rentó</th>
			      	
			  	</tr>

			  </tfoot>
			  </table> 
			  </div>

			  </div>
         </div>
     </div>
	
		</div>
		
		</div>
		
	</div>
</div>
<!--endlistado-->
	

	<!-- Modal Structure -->
	  <div id="galery" class="modal">
	    <div class="modal-content">
	    	<div class="modal-body">
	      <h5>Galeria de ({{$lugar->Titulo}})</h5>
	    
	      <hr>

	      <!--galery-->
	     <div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    @foreach($lugar->galeria as $oldpoplet)
    <li data-target="#myCarousel" data-slide-to="{{$loop->iteration}}"></li>

    @endforeach
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      <img src="{{ url('uploads/gestions/'.$lugar->imagen) }}" alt="Imagen Destacada">
    </div>
	@foreach($lugar->galeria as $oldpoplet)
    <div class="item">

      <img src="{{ url('uploads/gestions/poplets/'.$oldpoplet->gestion_id.'/'.$oldpoplet->imagen) }}" alt="">
    </div>

    @endforeach
    
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<hr>
		<div class="modal-footer">
	      <a href="#!" class="modal-action modal-close waves-effect waves-green btn" style="float: right;">Cancelar</a> 
			</div>
		</div>
	    </div>


	  </div>




</div>
@endsection

@section('scripts')
<script type="text/javascript" language="javascript" src="{{ url('js/advanced-datatable/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ url('js/data-tables/DT_bootstrap.js') }}"></script>
<!--dynamic table initialization -->
<script src="{{ url('js/dynamic_table_init.js') }}"></script>
<script>
	$(document).ready(function() {
		$('.table tr th:first-child').removeClass('sorting_desc');
		$('.table tr th:first-child').addClass('sorting');
		$('.table tr th:nth-child(3)').addClass('sorting_asc');
	});
	
</script>
@endsection