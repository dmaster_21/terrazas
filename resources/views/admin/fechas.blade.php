@extends('templates.admin')
@section('header')
<link rel="stylesheet" href="{{ url('js/data-tables/DT_bootstrap.css') }}" />
   <link rel="stylesheet" type="text/css" href="{{ url('fullcalendar/fullcalendar.min.css')}}">
            <link rel="stylesheet" href="{{ url('fullcalendar/lib/cupertino/jquery-ui.min.css')}}">       
@endsection
@section('pagecontent')
<div class=" main">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-6">
				<h3 class="">Fechas</h3>
			</div>
			<div class="col-md-6 text-right valign-wrapper" style="justify-content: space-between;">
				<div class="text-center" style="margin-left: auto; margin-top: 20px;">
					<a href="#nuevo" class="btn btn-primary right waves-effect waves-light btn-large modal-trigger"><i class="fa fa-plus"></i> Añadir fecha</a>
				</div>
				
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				@include('snip.notificaciones')
				@if($venues->isEmpty())
				<div class="alert alert-warning alert-dismissable">
				    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				    <ul>
				        <li>Aún no se han creado | Aprobado  lugares, te recomendamos ir a la sección <a href="{{url('/gestion_espacios')}}">lugares</a> y crear los necesarios.</li>
				    </ul>
				  </div>
				@endif
			</div>
		</div>
		<p>&nbsp;</p>

<div class="card">
		<div class="row">
		
				<div class="col-md-12">
					
				<div id="calendar"></div>
			</div>

		</div>
				

			
		</div>
				
		</div>
		
		
		
	</div>
</div>

@include('modals.modalfechas');


@endsection

@section('scripts')
<script type="text/javascript" language="javascript" src="{{ url('js/advanced-datatable/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ url('js/data-tables/DT_bootstrap.js') }}"></script>
<!--dynamic table initialization -->
<script src="{{ url('js/dynamic_table_init.js') }}"></script>
<script>
	$('#info2').modal('show');
	$(document).ready(function() {
		$('.table tr th:first-child').removeClass('sorting_desc');
		$('.table tr th:first-child').addClass('sorting');
		$('.table tr th:nth-child(3)').addClass('sorting_asc');
	});
	
</script>

  <script src="{{ url('fullcalendar/lib/jquery-ui.custom.min.js')}}"></script>
    <script src="{{ url('fullcalendar/lib/moment.min.js')}}"></script>
    <script src="{{ url('fullcalendar/fullcalendar.js')}}"></script>
    <script src="{{ url('fullcalendar/locale/es.js')}}"></script>
   

    <script>

    	function llenado($val){
    		$('#vhoras').val($val);
    	}
        $(document).ready(function() {
            var currentLangCode = 'es';//cambiar el idioma al español
 		
            $('#calendar').fullCalendar({
                eventClick: function(calEvent, jsEvent, view) { 

                	//$(this).attr('class', 'modal-trigger');
                	$(this).addClass('modal-trigger');
                },
 
                header: {
                    left: 'prev,next today myCustomButton',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
 
                lang:'es',
                editable: false,
                eventLimit: true, 
                events:[
                @foreach($fechas as $fecha)

                
                @if($fecha->status=="rentado")

                 {
     			 id :    '{{$fecha->id}}',
      			title  : '{{$fecha->nombre}}',
     		    start  : '{{$fecha->fecha}}',
                color  :  '#ef4b4b',
                url :'#info{{$fecha->id}}'

   				 },
					
				@endif
					
				@if($fecha->status=="apartado")
				 {
      				id :    '{{$fecha->id}}',
      				title  : '{{$fecha->nombre}}',
      				start  : '{{$fecha->fecha}}',
      				color  :  'Orange',
      				url :'#info{{$fecha->id}}'
   				 },
				@endif

				@if($fecha->status=="disponible")
                {
      				id :    '{{$fecha->id}}',
      				title  : '{{$fecha->nombre}}',
      				start  : '{{$fecha->fecha}}',
      				color  :  'green',
      				url :'#info{{$fecha->id}}'
   				 },
				@endif

			

   
    @endforeach
    ]

               
                
   
                

                });
   
        });


    </script>
@endsection