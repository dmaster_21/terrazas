@extends('templates.admin')

@section('pagecontent')
<div class=" main">
	<div class="container-fluid">
		<div class="row" >
			<div class="card">
    <div class="card-content">
    	<div class="row">
			<h3 class="text-center">Inspección de Reserva</h3>
		<hr>

		<div class="col-md-12">
				@include('snip.notificaciones')
				
@if($detalle)
<div class="col-md-12">
	<div class="card">
    <div class="card-content">
    	<div class="row">
    		<h5 class="text-center"><i class="fa fa-eye"></i> Datos de la Reservación</h5>
			@if($detalle->status=='disponible')
    		 <div class="alert alert-success alert-dismissable">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <i class="fa fa-check-circle"></i>
     Estimado Usuario El Lugar Se Encuentra Disponible. Desde Esta Opción Puede Emitir una Reserva Directa.
    </div>
    @endif
    	<div class="col-md-6">
    		<table class="table table-hover">
	<tr>
		<th>Nombre Lugar</th>
		<td>{{$detalle->nombre}}</td>
	</tr>

	<tr>
		<th>Fecha Publicada:</th>
		<td>{{$detalle->fecha}}</td>
	</tr>

	<tr>
		<th>Tipo:</th>
		<td>{{$detalle->tipo}}</td>
	</tr>

	<tr>
		<th>zona:</th>
		<td>{{$detalle->venue->zona->nombre}}</td>
	</tr>

	<tr>
		<th>Precio:</th>
		<td>{{number_format($detalle->precio,'2','.',',')}} MXN</td>
	</tr>

	<tr>
		<th>Capacidad:</th>
		<td>{{$detalle->capacidad}} </td>
	</tr>
		
	</table>
    		
    	</div>    
    	<!--ednd col6-->
    	<div class="col-md-6">

    		<form action="{{url('/reserva/editar') }}" method="POST" >
				{!! csrf_field() !!}
	<input type="hidden" name="id" id="" value="{{$detalle->id}}">
	<div class="col-md-12">
	<label>Condición</label> 
	<select name="condicion" id="condicion1" class="select">
		          	<option value="">Selecciona...</option>
		          	<option value="disponible">Disponible.</option>
		          	<option value="apartado">Apartado.</option>
		          	<option value="rentado">Rentado.</option>
	</select>	

                                         <script>
										document.getElementById('condicion1').value="{{$detalle->status}}";
									</script>

	</div>

	<div class="col-md-12">
	<label>Apartador:</label>
	<input type="text" name="cliente" value="{{$detalle->apartador}}">
	</div>

	<div class="col-md-12">
	<label>Nota:</label>
	<textarea id="nota" name="nota" class="materialize-textarea" required>{{$detalle->nota}}</textarea>
	
	</div>
</div>

<div class="col-md-12 text-center">
	
	<button type="submit" class="btn btn-default"><i class="fa fa-edit"></i>Guardar Cambios</button>
	
</div>
</div>
</form>
    		
    	</div>




    	</div>

    </div>
</div>
</div>

@endif

@endsection

@section('scripts')
<script type="text/javascript" language="javascript" src="{{ url('js/advanced-datatable/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ url('js/data-tables/DT_bootstrap.js') }}"></script>
<!--dynamic table initialization -->
<script src="{{ url('js/dynamic_table_init.js') }}"></script>
<script>
	$('#info2').modal('show');
	$(document).ready(function() {
		$('.table tr th:first-child').removeClass('sorting_desc');
		$('.table tr th:first-child').addClass('sorting');
		$('.table tr th:nth-child(3)').addClass('sorting_asc');
	});
	
</script>


   
@endsection