@extends('templates.admin')
@section('header')
<link rel="stylesheet" href="{{ url('js/data-tables/DT_bootstrap.css') }}" />
@endsection
@section('pagecontent')
<div class=" main">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-6">
				<h5 class="">Rentas En Efectivo sin Confirmar</h5>
			</div>
			<div class="col-md-6 text-right valign-wrapper" style="justify-content: space-between;">
				
				
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				@include('snip.notificaciones')
			</div>
		</div>
		<p>&nbsp;</p>

		<div class="col-md-12 card">
			<hr>
		<div class="row">
			<div class="col-md-12">
				<div class="adv-table table-responsive">
			  <table class="display table table-bordered table-striped table-hover" id="dynamic-table">
			  <thead>
			  	<tr>
					<th>Usuario</th>
			      	<th>Lugar</th>			      
			      	<th>Fecha Apartada</th>
					<th>Fecha Solicitud</th>
					<th>Fecha Anulación</th>
					<th>Monto Reserva</th>
					<th></th>
			  	</tr>
			  </thead>
			  <tbody>
			  	@foreach ($reserva as $key => $value)
			  		<tr>
					<td>{{$value->user->name}}</td>
			      	<td>{{$value->venue->Titulo}}</td>			      
			      	<td>{{ date("Y-d-m ", strtotime($value->fecha_reservada))}}</td>
					<td>{{$value->created_at}}</td>
					<td>{{date("Y-m-d h:m:s",strtotime($value->created_at."+ 1 days"))}}</</td>
					<td>{{number_format($value->costo_reserva,2,',','.')}} MXN</</td>
					<td><a class="waves-effect waves-light btn green modal-trigger" href="#edit{{$value->id}}" title="Reportar Pago"><i class="fa fa-edit"></i></a></td>
			  	</tr>
			  	@endforeach
			  	

			  </tbody>
			  <tfoot>
			  		<tr>
					<th>Usuario</th>
			      	<th>Lugar</th>			      
			      	<th>Fecha Reserva</th>
					<th>Fecha Solicitud</th>
					<th>Fecha Anulación</th>
					<th>Monto Reserva</th>
					<th></th>
			  	</tr>
			  </tfoot>
			  </table>

			  </div>
			</div>
				
		</div>
		
		
	</div>
	</div>
</div>
@foreach ($reserva as $key => $value)

<!-- Modal Structure -->
	  <div id="edit{{$value->id}}" class="modal">
	    <div class="modal-content">
	      <h4>Reportar Pago En Efectivo</h4>	      

	    
			<form action="{{action('ReservasController@efectivo')}}" method="POST" >

				
				<input type="hidden" name="_token" value="{{ csrf_token()}}">
				<input type="hidden" name="id_reserva" value="{{$value->id}}">

				<div class="col-md-12">
					<label for="fecha">Nombre del Cliente</label>
		          <input  type="text" class="validate" value="{{$value->user->name}}">
		          
		        </div>

		        <div class="col-md-12">
					<label for="fecha">Estatus Del Lugar</label>
		          <input  name="status" type="text" value="rentado" readonly="">
		          
		        </div>

				<div class="col-md-12">
					<label for="fecha" type="button" class="btn btn-yellow "><strong><i class="fa fa-calendar"></i></strong></label>

		          <input id="fecha" name="fecha" type="text" class="validate datepicker" value="{{old('fecha')}}" required placeholder="Fecha de Pago">
		          
		        </div>
		       



		         <div class="col-md-12">
		          <label for="fecha">Nota de Apartado o Reserva:</label>
		        <textarea id="nota" name="nota" class="materialize-textarea" required>{{old('nota')}}</textarea>
		        
		        </div>

		        <div class="col-md-12 text-center">
		        	<button type="submit" class="btn btn default">Reportar Pago</button>
		        	 <a href="#!" class="modal-action modal-close waves-effect waves-green btn" >Cancelar</a>
		        </div>
				
			</form>
		
			<p>	&nbsp;</p>
	    </div>


	  </div>





@endforeach


 


@endsection

@section('scripts')
<script type="text/javascript" language="javascript" src="{{ url('js/advanced-datatable/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ url('js/data-tables/DT_bootstrap.js') }}"></script>
<!--dynamic table initialization -->
<script src="{{ url('js/dynamic_table_init.js') }}"></script>
<script>
	$(document).ready(function() {
		$('.table tr th:first-child').removeClass('sorting_desc');
		$('.table tr th:first-child').addClass('sorting');
		$('.table tr th:nth-child(3)').addClass('sorting_asc');
	});
	
</script>
@endsection