@component('mail::message')
#  Saludos  {{ $reserva->user->name }}

Tenemos el Agrado de informarte que el lugar ({{ $reserva->venue->Titulo }}) el cual Apartaste 
Para la Fecha {{ date("d/m/Y ", strtotime($reserva->fecha_reservada))}} ha Sido Liberado, Te Invitamos a Culminar tu Reservación Lo antes Posible, Debido a Que Pueden Existir Otros Usuarios En Lista de espera para este Lugar.

@component('mail::button', ['url' => 'Test.migrard.mx/terrazas/confirmaReserva/'.$reserva->tokens, 'color' => 'green'])
Procesar Reserva
@endcomponent


@endcomponent
