@component('mail::message')
# Hola!   {{ $reserva->user->name }}

Tu Renta del Espacio ({{ $reserva->venue->Titulo }}) del Dia : {{ date("d/m/Y ", strtotime($reserva->fecha_reservada))}} Ha Sido Anulada, en Vista de Que se Cumplen 24 Horas de Tu Solicitud y el  Pago En Efectivo No Fue Reportado. Muchas Gracias. !!

@endcomponent