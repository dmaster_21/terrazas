@component('mail::message')
# Bienvenido  {{ $user->name }}

Tu Solicitud de Publicación deL Lugar en nuestra Plataforma {{ config('app.name') }}.  Ha Sido Procesada de Manera Satisfactoria. Ingresa Con Tu Email y Contraseña suministrados.



@component('mail::button', ['url' =>'Test.migrard.mx/terrazas/entrar' , 'color' => 'green'])
INGRESAR 
@endcomponent

@endcomponent