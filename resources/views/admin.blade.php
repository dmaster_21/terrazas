@extends('templates.admin')

@section('pagecontent')
<div class=" main">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6">
        <h3 class="">Escritorio</h3>
      </div>
    </div>
<form action="{{url('admin')}}" method="post">
          {!! csrf_field() !!}
    <div class="row">
    

      <div class="col-md-2 ">                
              <input type="text" id="desde" class="form-control datepicker browser-default" name="from" placeholder="Desde..." value="{{$from}}">
          </div>
           <div class="col-md-2 ">  
           <label for="desde" type="button" class="btn btn-yellow "><strong><i class="fa fa-calendar"></i></strong></label>              
              
          </div>
          <div class="col-sm-3 col-md-2">
              <input type="text" id="hasta" class="form-control datepicker browser-default" name="to" placeholder="Hasta..." value="{{$to}}">
          </div>
          <div class="col-sm-2">                
              <label for="hasta" type="button" class="btn btn-yellow "><strong><i class="fa fa-calendar"></i></strong></label>
          </div>
          <div class="col-sm-3 col-md-2">
              <input type="submit" value="Ver periodo" class="btn btn-success">
          </div>

    
    </div>
    </form>
    <p>&nbsp;</p>

    

        
    <div class="row">
      <div class="col-md-4">
        <div class="card  darken-1">
                <div class="card-content">
                  <span class="card-title">Usuarios</span>

                  <h5 style="font-weight: 700">{{$usuarios}} Usuarios registrados</h5>

                  <canvas id="usuariosbar" width="400" height="400"></canvas>


                </div>
                
            </div>
      </div>
       <div class="col-md-8">
        <div class="card  darken-1">
                <div class="card-content">
                  <span class="card-title"></span>

                  <h5 style="font-weight: 700">Cantidad Lugares Por Zona</h5>

                  <canvas id="lineChart"></canvas>


                </div>
                
            </div>
      </div>

        <div class="col-md-6">
        <div class="card  darken-1">
                <div class="card-content">
                  <span class="card-title"></span>

                  <h5 style="font-weight: 700">Rentas de Lugares</h5>

                  <canvas id="polarChart"></canvas>


                </div>
                
            </div>
      </div>


       <div class="col-md-6">
        <div class="card  darken-1">
                <div class="card-content">
                  <span class="card-title"></span>

                  <h5 style="font-weight: 700">Lugares Vistos | Solicitados</h5>

                  <canvas id="horizontalBar"></canvas>


                </div>
                
            </div>
      </div>
      
      
    </div>

    


  </div>
</div>
@endsection

@section('scripts')
<script>
var ctx = document.getElementById("usuariosbar").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["Hombres","Mujeres" ],
        datasets: [{
            label: '# Hombres',
            data: [{{$hombres}}],
            backgroundColor: [
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        },
        {
            label: '# Mujeres',
            data: [{{$mujeres}}],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        },
        ]
    },
    options: {
      legend: {
        display: false
    },
        scales: {
            yAxes: [{
              barThickness : 30,
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});

 var ctxL = document.getElementById("lineChart").getContext('2d');
  var myLineChart = new Chart(ctxL, {
    type: 'line',
    data: {
      labels: [
      @foreach ($zona as $zonas)
        "{{$zonas->nombre}}",
      @endforeach
     
      ],
      datasets: [
        {
          label: "Lugares en La Zona",
          data: [
           @foreach ($zona as $zonas)
        "{{$zonas->qty}}",
      @endforeach
          ],
          backgroundColor: [
            'rgba(0, 137, 132, .2)',
          ],
          borderColor: [
            'rgba(0, 10, 130, .7)',
          ],
          borderWidth: 2
        }
      ]
    },
    options: {
      responsive: true
    }
  });


    //polar
  var ctxPA = document.getElementById("polarChart").getContext('2d');
  var myPolarChart = new Chart(ctxPA, {
    type: 'polarArea',
    data: {
      labels: ["Totales", "Pagadas", "Lista de Espera"],
      datasets: [{
        data: [{{$totales}},{{$rentadas}},{{$espera}}],
        backgroundColor: ["rgba(219, 0, 0, 0.1)", "rgba(0, 165, 2, 0.1)", "rgba(255, 195, 15, 0.2)",
          "rgba(55, 59, 66, 0.1)", "rgba(0, 0, 0, 0.3)"
        ],
        hoverBackgroundColor: ["rgba(219, 0, 0, 0.2)", "rgba(0, 165, 2, 0.2)",
          "rgba(255, 195, 15, 0.3)", "rgba(55, 59, 66, 0.1)", "rgba(0, 0, 0, 0.4)"
        ]
      }]
    },
    options: {
      responsive: true
    }
  });

 new Chart(document.getElementById("horizontalBar"), {
    "type": "horizontalBar",
    "data": {
      "labels": [
      @foreach($vistos as $visto)
      "{{$visto->Titulo}}",
      @endforeach
      ],
      "datasets": [{
        "label": "#Lugar Solicitado",
        "data": [
          @foreach($vistos as $visto)
      "{{$visto->qty}}",
      @endforeach
        ],
        "fill": false,
        "backgroundColor": ["rgba(255, 99, 132, 0.2)", "rgba(255, 159, 64, 0.2)",
          "rgba(255, 205, 86, 0.2)", "rgba(75, 192, 192, 0.2)", "rgba(54, 162, 235, 0.2)",
          "rgba(153, 102, 255, 0.2)", "rgba(201, 203, 207, 0.2)"
        ],
        "borderColor": ["rgb(255, 99, 132)", "rgb(255, 159, 64)", "rgb(255, 205, 86)",
          "rgb(75, 192, 192)", "rgb(54, 162, 235)", "rgb(153, 102, 255)", "rgb(201, 203, 207)"
        ],
        "borderWidth": 1
      }]
    },
    "options": {
      "scales": {
        "xAxes": [{
          "ticks": {
            "beginAtZero": true
          }
        }]
      }
    }
  });
</script>
@endsection