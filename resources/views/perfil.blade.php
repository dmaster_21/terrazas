
@extends('templates.default')

@section('scripts')

    <script type="text/javascript">

        $('#img_perfil').change(function () {
            $('#foto').submit();
        });

    </script>
@endsection


@section('pagecontent')

<div class="container bootstrap snippet">
    <div class="row">
  		<div class="col-sm-10"><h1>{{$user->name}}</h1></div>
        <div class="col-sm-2">
            <form class="form-horizontal" role="form" method="POST" id="foto"  action="{{route('Miperfil-update-image')}}"  enctype="multipart/form-data">
                {!! csrf_field() !!}
            <label for="img_perfil"><img title="profile image" class="img-circle img-responsive" src="{{url($user->avatar)}}">
                <input style="display: none;" type="file" name="img_perfil" id="img_perfil"> </label>
            </form>
        </div>
    	<!--<a href="#" class="pull-right"><img title="profile image" class="img-circle img-responsive" src="http://www.gravatar.com/avatar/28fd20ccec6865e2d5f0e1f4446eb7bf?s=100"></a></div>-->

    </div>
    <div class="row">
  	@include('templates.navCliente')
    	<div class="col-sm-9">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#home"> <i class="fa fa-user"></i>Mi Información</a></li>
               <li ><a  href="{{url('/reservaClient')}}"><i class="fa fa-calendar"></i> Reservaciones</a></li>
              </ul>
  <div class="row">
      <div class="col-md-12">
        @include('snip.notificaciones')
      </div>
    </div>
 

              
          <div class="tab-content">
            <div class="tab-pane active" id="home">
                <hr>
                  <form class="form" action="{{action('UsuarioController@update',$user->id)}}" method="GET" id="registrationForm" novalidate="">
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="first_name"><h5>Nombre Completo</h5></label>
                              <input type="text" class="form-control" name="nombre" id="first_name" placeholder="Nombre Completo" value="{{$user->name}}" title="Tu nombre completo." required="">
                          </div>
                      </div>

          
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="phone"><h5>Telefono</h5></label>
                              <input type="text" class="form-control" name="telefono" id="phone" placeholder="Telefono" value="{{$user->tel}}" title="Tu telefono." required="">
                          </div>
                      </div>
          
                      <!--<div class="form-group">
                          <div class="col-xs-6">
                             <label for="mobile"><h5>Mobile</h5></label>
                              <input type="text" class="form-control" name="mobile" id="mobile" placeholder="enter mobile number" title="enter your mobile number if any.">
                          </div>
                      </div>-->
                      <div class="form-group">
                          
                          <div class="col-xs-12">
                              <label for="email"><h5>Email</h5></label>
                              <input type="email" class="form-control" name="email" id="email" placeholder="you@email.com" value="{{$user->email}}" title="Tu email." required="">
                          </div>
                      </div>

                       <div class="form-group">
                          
                          <div class="col-xs-12">
                              <label for="fecha_n"><h5>Fecha Nac.</h5></label>
                              <input name="fecha" type="text" class="validate datepicker" id="fecha_n" placeholder="Fecha Nac" value="{{$user->dob}}" required="">
                          </div>
                      </div>
                      <!--<div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="email"><h5>Dirección</h5></label>
                              <input type="email" class="form-control" id="location" placeholder="somewhere" title="enter a location">
                          </div>
                      </div>-->
                    

                       <div class="form-group">
                          
                          <div class="col-xs-12">
                              <label for="empresa"><h5>Empresa</h5></label>
                              <input type="text" class="validate" name="empresa" id="empresa" value="{{$user->empresa}}" title="Empresa" required="">
                          </div>
                      </div>
                      <!--<div class="form-group">
                          
                          <div class="col-xs-6">
                            <label for="password2"><h5>Verify</h5></label>
                              <input type="password" class="form-control" name="password2" id="password2" placeholder="password2" title="enter your password2.">
                          </div>
                      </div>-->
                      <!--<div class="form-group">
                           <div class="col-xs-12">
                                <br>
                              	<button class="btn btn-lg btn-success" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                               	<button class="btn btn-lg" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>
                            </div>
                      </div>-->
              	

                <div class="col-xs-12 text-center">
                  <button class="btn btn-default"><i class="fa fa-edit"></i> Guardar Cambios</button>
                  <a class="waves-effect waves-light btn modal-trigger" href="#clave" title="Destacar"><i class="fa fa-key"></i>  Cambiar Contraseña</a>

                  </form>
                </div>
              
              <hr>
              
             </div><!--/tab-pane-->
             <div class="tab-pane" id="messages">
               
               <h2></h2>
               
               <hr>
                  <form class="form" action="##" method="post" id="registrationForm">
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="first_name"><h5>First name</h5></label>
                              <input type="text" class="form-control" name="first_name" id="first_name" placeholder="first name" title="enter your first name if any.">
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                            <label for="last_name"><h5>Last name</h5></label>
                              <input type="text" class="form-control" name="last_name" id="last_name" placeholder="last name" title="enter your last name if any.">
                          </div>
                      </div>
          
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="phone"><h5>Phone</h5></label>
                              <input type="text" class="form-control" name="phone" id="phone" placeholder="enter phone" title="enter your phone number if any.">
                          </div>
                      </div>
          
                      <div class="form-group">
                          <div class="col-xs-6">
                             <label for="mobile"><h5>Mobile</h5></label>
                              <input type="text" class="form-control" name="mobile" id="mobile" placeholder="enter mobile number" title="enter your mobile number if any.">
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="email"><h5>Email</h5></label>
                              <input type="email" class="form-control" name="email" id="email" placeholder="you@email.com" title="enter your email.">
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="email"><h5>Location</h5></label>
                              <input type="email" class="form-control" id="location" placeholder="somewhere" title="enter a location">
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="password"><h5>Password</h5></label>
                              <input type="password" class="form-control" name="password" id="password" placeholder="password" title="enter your password.">
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                            <label for="password2"><h5>Verify</h5></label>
                              <input type="password" class="form-control" name="password2" id="password2" placeholder="password2" title="enter your password2.">
                          </div>
                      </div>
                      <div class="form-group">
                           <div class="col-xs-12">
                                <br>
                              	<button class="btn btn-lg btn-success" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                               	<button class="btn btn-lg" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>
                            </div>
                      </div>
              	</form>
               
             </div><!--/tab-pane-->
             <div class="tab-pane" id="settings">
            		
               	
                 
              </div>
               
              </div><!--/tab-pane-->
          </div><!--/tab-content-->

        </div><!--/col-9-->
    </div><!--/row-->


    <div id="clave" class="modal">
      <div class="modal-content">
        <h4>Modificar Contraseña</h4>
        <p>¿Está seguro que desea Cambiar su contraseña ?</p>

       
      <form action="{{action('UsuarioController@reset',$user->id)}}" method="POST" novalidate="">

         <div class="md-form input-field">
                             <label for="defaultForm-pass"><i class="fa fa-lock grey-text fa-lg"></i> Contraseña</label>
                            <input type="password" name="password" id="defaultForm-pass" class="form-control validate" required>
                           
                        </div>
                        <div class="md-form input-field">
                               <label for="defaultForm-pass-confirm"><i class="fa fa-lock grey-text fa-lg"></i> Confirmar contraseña</label>
                            <input type="password" name="password_confirmation" id="defaultForm-pass-confirm" class="form-control validate" required>
                         
                        </div>

        <input type="hidden" name="_token"value="{{ csrf_token()}}">

        <input type="submit" class="modal-action modal-close waves-effect waves-green btn" value="Cambiar Clave" style="float: right;">

         <a href="#!" class="modal-action modal-close waves-effect waves-green red btn" style="float: right;">Cancelar</a> 

      
      </form>
      <p> &nbsp;</p>
      </div>


    </div>


@endsection