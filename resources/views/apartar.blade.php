
@extends('templates.default')


@section('header')

@endsection
@section('pagecontent')
<section class="jumbotron text-center">
    <div class="container">
        <h1 class="jumbotron-heading">RESERVACIÓN</h1>
     </div>
</section>

<div class="container mb-4">

	<div class="row">
			<div class="col-md-12">
				@include('snip.notificaciones')
			</div>
		</div>

     
     

          @if($aparta)
        
          
           
            <div class="col-12">
             
            <hr>

            <form action="{{action('ReservasController@apartar')}}"  method="GET">
              {!!csrf_field()!!}
              <input type="hidden" name="horario_id" value="{{$aparta->id}}">
                <div class="table-responsive">
                    
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                  
                                    <th scope="col">Lugar</th>
                                    <th scope="col">Fecha</th>
                                    <th scope="col">Zona</th>
                                    <th scope="col">Tipo</th>
                                    <th scope="col">Capacidad</th>
                                    <th scope="col">Precio</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                              
                             
                                <tr>
                                    
                                    <td>{{$aparta->nombre}}</td>
                                    <td>{{$aparta->fecha}}</td>
                                    <td>{{$aparta->venue->zona->nombre}}</td>
                                    <td>{{$aparta->tipo}}</td>
                                    <td>{{$aparta->capacidad}}</td>
                                    <td>${{number_format($aparta->precio,2,',','.')}} MXN</td>
                                  
                                 
                                </tr>
                            
                                
                                
                                

                                <tr>
                                    <td></td>

                                </tr>

                                

                            </tbody>
                            <thead>
                          
                            </thead>
                        </table>

 
                    
                    
                </div>
            </div>
            <div class="">
                <div class="row">                  
                
                           
                   
                    <div class="col-xs-12  col-sm-4">
                        <a href="{{url('/lugar/'.$aparta->venue_id)}}"  class="btn btn-lg  btn-success text-uppercase"><i class="fa fa-chevron-left"></i>Volver </a>
                    </div>
                     <div class="col-xs-12  col-sm-4">
                    </div>


                    <div class="col-xs-12  col-sm-4">
                        <button class="btn btn-lg  btn-success text-uppercase"><i class="fa fa-check-circle"></i> Procesar Solicitud</button>
                    </div>

                     
                </div>
            </div>
        </form>
        @else
        
        <div class="table-responsive">
                    
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Lugar</th>
                                    <th scope="col">Fecha</th>
                                    <th scope="col">Zona</th>
                                    <th scope="col">Tipo</th>
                                    <th scope="col">Capacidad</th>
                                    <th scope="col">Precio</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td colspan="7">
                                    <center>
                                    <h6>Estimado Usuario Usted Aún No Ha Seleccionado Un Lugar Para rentar!!</h6>
                                </center>
                                    
                                </td>
                            </tr>
                             <tr>
                                    <td></td>

                                    <td><h5><strong>Total</strong></h5></td>
                                    <td colspan="5" class="text-right"> <h5><strong> ${{number_format($total,2,',','.')}}</span> MXN</strong></h5> </td>
                                </tr>

                            </tbody>
                        </table>
                    
                     </div>    

                        <center>
                     <div class="col-md-4">
                        <a href="{{url('/')}}">
                        <button class="btn btn-lg  btn-success text-uppercase"><i class="fa fa-chevron-left"></i> Ver Más Lugares</button>
                    </a>
                   
                    </div>     
                        </center>


        @endif
    </div>
</div>
@endsection