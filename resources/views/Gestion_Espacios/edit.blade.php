@extends('templates.admin')

@section('pagecontent')
<div class=" main">
	<div class="container-fluid">
		
		<form action="{{action('GestionEspaciosController@update',$lugar->id)}}" method="POST" enctype="multipart/form-data" id="FrVenue" novalidate="">
		<input type="hidden" name="_method" value="PUT">
		<input type="hidden" name="_token"value="{{ csrf_token()}}">
			
			
		<div class="row">
			<div class="col-md-6">
				<h4 class="">Editar Lugar ({{$lugar->Titulo}})</h4>
			</div>
			<div class="col-md-6 text-right valign-wrapper" style="justify-content: space-between;">
				<div class="text-center" style="margin-left: auto; margin-top: 20px;">
					<input type="submit" value="Guardar Cambios" class="btn btn-primary right waves-effect waves-light btn-large">
				</div>
				
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				@include('snip.notificaciones')
				@if(!$servicios)
				<div class="alert alert-warning alert-dismissable">
				    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				    <ul>
				        <li>Aún no se han creado servicios, te recomendamos ir a la sección <a href="{{url('/servicios')}}">servicios</a> y crear los necesarios.</li>
				    </ul>
				  </div>
				@endif
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-8">
				
				    	<div class="col s12">
					      <div class="row">
					      	
					        <div class="input-field col col-md-6">
					        	 <label for="titulo" >Título</label>
                            
					          <input id="titulo" name="titulo" type="text" class="validate" value="{{$lugar->Titulo}}" required>
					        
					        </div>
					        <div class="input-field col col-md-6">
									<select name="zona" id="zona1" class="select" required>
										<option value="">Selecciona</option>
										@foreach($zonas as $zona)
										<option value="{{$zona->id}}">{{$zona->nombre}}</option>
										@endforeach
									</select> 
									<label for="zona">Zona</label>
									<script>
										$('#zona1').val({{$lugar->zona_id}});
									</script>
					        </div>
					      </div>
					      <div class="row">
					        <div class="input-field col col-md-6">
					        	<label for="capacidad"  >Capacidad</label>
					          <input id="capacidad" name="capacidad" type="number" class="validate" value="{{$lugar->capacidad}}" required>
					          
					        </div>
					        <div class="input-field col col-md-6">
					          <select name="tipo" id="tipo2" class="select validate" required>
					          	<option value="">Selecciona</option>
					          	<option value="Salón">Salón</option>
					          	<option value="Terraza">Terraza</option>
					          	<option value="Jardín">Jardín</option>
					          </select>
					          <label for="tipo2" style="margin-top: -15px" >Tipo</label>
					          <script>
						        //	document.getElementByID('tipo').value="{{$lugar->tipo}}";
						       $('#tipo2').val('{{$lugar->tipo}}');
						     
						        </script>
					        </div>
					      </div>
					      <div class="row">
					        <div class="input-field col s12">
					        	<label for="descripcion" style="margin-top: -15px" >Descripción</label>
					        	
					          <textarea id="descripcion" name="descripcion" class="materialize-textarea" required>{{$lugar->descripcion}}</textarea>
					          
					        </div>
					      </div>
					      <div class="row">
					        <div class="input-field col s12">
					        	<label for="direccion" style="margin-top: -15px" >Dirección</label>
					        	
					          <textarea id="direccion" name="direccion" class="validate materialize-textarea" required>{{$lugar->direccion}}</textarea>
					          
					        </div>
					      </div>
					      <div class="row">

					      	<div class="input-field col col-md-6">
					      	 <label for="longitud" style="margin-top: -15px" >Télefono</label>
					          <input id="telefono" name="telefono" type="tel" class="validate number" value="{{$lugar->telefono_lugar}}" required>
					          
					        </div>

					         <div class="input-field col col-md-6">
					         <label for="longitud" style="margin-top: -15px" >Tamaño m3</label>
					          <input id="tamaño" name="tamaño" type="tel" class="validate number" value="{{$lugar->tamaño}}" required>
					          
					        </div>

					        <div class="input-field col col-md-6">
					        	<label for="latitud" style="margin-top: -15px" >Latitud</label>
					          <input id="latitud" name="latitud" type="tel" class="validate number" value="{{$lugar->latitud}}" required>
					          
					        </div>
					        <div class="input-field col col-md-6">
					        <label for="longitud" style="margin-top: -15px" >Longitud</label>
					          <input id="longitud" name="longitud" type="tel" class="validate number" value="{{$lugar->longitud}}" required>
					          
					        </div>

					        
					      </div>
					      <div class="row">
					        <div class="input-field col s12">
					        	<label for="reglamento" style="margin-top: -15px" >Reglamento</label>
					        	
					          <textarea id="reglamento" name="reglamento" class="materialize-textarea" required>{{$lugar->reglamento}}</textarea>
					          
					        </div>
					      </div>	
					    
					      
					    </div>


				
			
			</div>

			<div class="col-md-4">

				    <h5>Servicios incluidos</h5>
				    <div>
				    	<?php $check="";?>
				    	@if($servicios)
				    	@foreach($servicios as $servicio)	

				    			    	     		

			
					      <div class="row">
					        <div class="col s12">
					          <p>
							      <input type="checkbox" name="servicio[]"  value="{{$servicio->id}}" id="cat{{$servicio->id}}"

								 @foreach($servch as $ser)
								 @if($ser->id==$servicio->id)
                      				checked="checked"
                      				@endif
                           @endforeach	
							       />
							      <label for="cat{{$servicio->id}}">{{$servicio->nombre}}</label>
						      </p>
					        </div>
					      </div>
					    @endforeach
					    @endif

					  
				    </div>


				    
					<hr>
						<div class="alert alert-warning alert-dismissable">
				    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				    <ul>
				        <li>Estimado Usuario Te Recordamos que Solo Podras Ingresar Imagén en formato <b>JPG</b> y <b>PNG</b> El sistema solo Cambiara sus Imagenes Si detecta algun archivo Cargado.</li>
				    </ul>
				  </div>		
				
				    <h5>Imágen destacada</h5>
				    <div>
				    	<div class="row">
					      
					       
					      <div class="file-field input-field">
						      <div class="btn">
						        <span>Subir</span>
						        <input type="file" name="imagen">
						      </div>
						      <div class="file-path-wrapper">
						        <input class="file-path validate" type="text">
						      </div>
						    </div>
				    </div>

				  


				
				  

				     <h5><a class="modal-trigger" href="#view">Galería</a></h5> 
				   

				    <div class="popletsinput">
				      <div class="file-field input-field poplet1">
					      <div class="btn">
					        <span>Subir</span>
					        <input type="file" name="poplet[]">
					      </div>
					      <div class="file-path-wrapper">
					        <input class="file-path validate" type="text">
					      </div>
					    </div>

					    <div class="file-field input-field poplet2" style="display: none;">
					      <div class="btn">
					        <span>Subir</span>
					        <input type="file" name="poplet[]">
					      </div>
					      <div class="file-path-wrapper">
					        <input class="file-path validate" type="text">
					      </div>
					    </div>


					    <div class="file-field input-field poplet3" style="display: none;">
					      <div class="btn">
					        <span>Subir</span>
					        <input type="file" name="poplet[]">
					      </div>
					      <div class="file-path-wrapper">
					        <input class="file-path validate" type="text">
					      </div>
					    </div>

					    <div class="file-field input-field poplet4" style="display: none;">
					      <div class="btn">
					        <span>Subir</span>
					        <input type="file" name="poplet[]">
					      </div>
					      <div class="file-path-wrapper">
					        <input class="file-path validate" type="text">
					      </div>
					    </div>

				    </div>
				    
				    <div class="text-right popletscontrols">
				    	<a class="minus" style="display: none;" onclick="popletremove();"><i class="fa fa-minus fa-2x" aria-hidden="true"></i></a>
				    <a class="plus" onclick="popletappend();"><i class="fa fa-plus fa-2x" aria-hidden="true"></i></a>
				    </div>
				   


				<script>
					var poplet=1;
					function popletappend(){
						poplet++;
						$( ".poplet"+poplet ).fadeIn();
						
						$('.minus').fadeIn();
						$('.popletsnum').val(poplet);
						if(poplet>=4){
							$('.plus').fadeOut();
						}
					}
					function popletremove(){
						$( ".poplet"+poplet ).fadeOut();
						poplet--;
						if(poplet<2){
							$('.minus').fadeOut();
						}
						if(poplet<5){
							$('.plus').fadeIn();
						}
						$('.popletsnum').val(poplet);
					}
				</script>




			</div>
		</div>
		
	</div>
	</form>
</div>


<!--Modal galery-->

<div id="view" class="modal">
	    <div class="modal-content">
	    	<center><h5> Galeria de {{$lugar->Titulo}}</h5></center>
	        <div class="modal-body">
	        	@if(count($lugar->galeria)<2)

	        	<div class="alert alert-warning alert-dismissable">
				    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				    <ul>
				        <li>Estimado Usuario El Lugar Debe Poseer Al Menos 1 Imágen. En Galeria Con el fin de Poder Brindar Al Cliente Una mejor Referencia del Lugar.</li>
				    </ul>
				  </div>

	        				 
	              @endif
	        	<table class="table table-striped">
	        		<thead>
	        		<tr>
	        			<th>#</th>
	        			<th><center>Imagén</center></th>
	        			<th></th>
	        		   </tr>
	        		</thead>
	        		<tbody>
	        			@foreach($lugar->galeria as $oldpoplet)
						<tr>
	        			<td><center>{{$loop->iteration}}</center></td>
	        			<td>
	        				<center>
	        				<img src="{{ url('uploads/gestions/poplets/'.$oldpoplet->gestion_id.'/'.$oldpoplet->imagen) }}" class="img-thumbnail materialboxed" style="width: 30%;">
	        			</center>
	        		</td>
	        			<td>
	        				<a href="{{ url('eliminar-img/'.$oldpoplet->id) }}" class="waves-effect waves-light btn red"
							@if(count($lugar->galeria)<2)
	        				 disabled
	        				 @endif

	        				 >
							<i class="fa fa-trash"></i>
							Eliminar
	        				</a>
	        			</td>
	        		   </tr>



	        			@endforeach
	        		</tbody>
	        		
	        	</table>
	      	
	         </div>

	         <div class="modal-footer">
	         	 <a href="#!" class="modal-action modal-close waves-effect waves-green btn" style="float: right;">Cancelar</a> 
	         </div>
	    </div>


	  </div>

<!--end modal-->
@endsection

@section('scripts')


<script>
$("#FrVenue" ).validate();

  jQuery.validator.messages.required = 'Esta campo es obligatorio.';
  jQuery.validator.messages.number = 'Esta campo debe ser num&eacute;rico.';
</script>
@endsection