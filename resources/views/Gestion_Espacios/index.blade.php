@extends('templates.admin')
@section('header')
<link rel="stylesheet" href="{{ url('js/data-tables/DT_bootstrap.css') }}" />
@endsection
@section('pagecontent')
<div class=" main">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-6">
				<h3 class="">Gestion de Lugares</h3>
			</div>
			<div class="col-md-6 text-right valign-wrapper" style="justify-content: space-between;">
				<div class="text-center" style="margin-left: auto; margin-top: 20px;">
					<a href="{{action('GestionEspaciosController@create')}}" class="btn btn-primary right waves-effect waves-light btn-large"><i class="fa fa-plus"></i> NUEVO</a>
				</div>
				
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				@include('snip.notificaciones')
			</div>
		</div>
		<p>&nbsp;</p>

		<div class="col-md-12 card">
			<hr>
		<div class="row">
			<div class="col-md-12">
				<div class="adv-table table-responsive">
			  <table class="display table table-bordered table-striped table-hover" id="dynamic-table">
			  <thead>
			  		<tr>
			      	<th>Nombre</th>
			      	<th>Zona</th>			      	
			      	<th>Capacidad</th>
					<th>Tipo</th>
					<th></th>
			  	</tr>
			  </thead>
			  <tbody>
			  	@if($index)
			  		@foreach($index as $venue)

						<tr>
							<td>{{$venue->Titulo}}</td>
							<td>{{$venue->zona->nombre}}</td>
					
							<td>{{$venue->capacidad}}</td>
							<td>{{$venue->tipo}}</td>
							<td class="text-center">
								<a class="waves-effect waves-light btn modal-trigger" href="{{url('gestion_espacios/')}}/{{$venue->id}}/edit" title="Editar Lugar"><i class="fa fa-pencil"></i></a>
								<a class="waves-effect waves-light btn red modal-trigger" href="#delete{{$venue->id}}" title="Eliminar Lugar"><i class="fa fa-times-circle"></i></a>
								<a class="waves-effect waves-light btn orange modal-trigger" href="{{url('gestion_espacios')}}/{{$venue->id}}" title="Ver Información"><i class="fa fa-eye"></i></a>
								@if(Auth::user()->role=="admin")
								@if($venue->destacado==1)
								<a class="waves-effect waves-light btn green modal-trigger" href="{{url('destacar')}}/{{$venue->id}}" title="Destacar"><i class="fa fa-star"></i></a>
								@else
								<a class="waves-effect waves-light btn modal-trigger" href="{{url('destacar')}}/{{$venue->id}}" title="Destacar"><i class="fa fa-star-half"></i></a>
									
								@endif
								@endif
				
								

							</td>	

						</tr>
					@endforeach
				@else
					<tr style="cursor: pointer;">
						<td></td>
						<td></td>
						<td></td>
						
						<td></td>
						<td></td>						
					</tr>

				@endif
				



			  </tbody>
			  <tfoot>
			  	<tr>
			      	<th>Nombre</th>
			      	<th>Zona</th>
			      	
			      	<th>Capacidad</th>
					<th>Tipo</th>
					<th></th>
			  	</tr>
			  </tfoot>
			  </table>

			  </div>
			</div>
				
		</div>
		
		
	</div>
	</div>
</div>



  @if($index)
	@foreach($index as $venue)
	<!-- Modal Structure -->
	  <div id="delete{{$venue->id}}" class="modal">
	    <div class="modal-content">
	      <h4>Eliminar lugar ({{$venue->Titulo}})</h4>
	      <p>¿Está seguro que desea eliminar este lugar?</p>

	      <a href="#!" class="modal-action modal-close waves-effect waves-green btn" style="float: right;">Cancelar</a> 
			<form action="{{action('GestionEspaciosController@destroy',$venue->id)}}" method="POST" >

				<input type="hidden" name="_method"value="DELETE">
				<input type="hidden" name="_token"value="{{ csrf_token()}}">
				<input type="submit" class="modal-action modal-close waves-effect waves-green red btn" value="Eliminar" style="float: right;">
			</form>
			<p>	&nbsp;</p>
	    </div>


	  </div>

	  @endforeach
	@endif



@endsection

@section('scripts')
<script type="text/javascript" language="javascript" src="{{ url('js/advanced-datatable/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ url('js/data-tables/DT_bootstrap.js') }}"></script>
<!--dynamic table initialization -->
<script src="{{ url('js/dynamic_table_init.js') }}"></script>
<script>
	$(document).ready(function() {
		$('.table tr th:first-child').removeClass('sorting_desc');
		$('.table tr th:first-child').addClass('sorting');
		$('.table tr th:nth-child(3)').addClass('sorting_asc');
	});
	
</script>
@endsection