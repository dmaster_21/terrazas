<div id="nuevo" class="modal">
  	<form action="{{ url('/agregar-fecha') }}" method="post" enctype="multipart/form-data">
	    <div class="modal-content">
	      <h4>Añadir nuevo</h4>
				{!! csrf_field() !!}

				<div class="col-md-12">
					<label for="venuen">Lugar</label>
					<br>
		          <select name="venue_id" id="venuen" class="select" required="">
		          	<option value="">Selecciona...</option>
		          	@foreach($venues as $venue)
						<option value="{{$venue->id}}">{{$venue->Titulo}}</option>
		          	@endforeach
		          </select>
		          
		        </div>
				<div class="col-md-12">
					 <label for="fecha" type="button" class="btn btn-yellow "><strong><i class="fa fa-calendar"></i></strong></label>
					

		          <input id="fecha" name="fecha" type="text" class="validate datepicker" value="{{old('fecha')}}" placeholder="Fecha a Publicar" required readonly="">
		          
		        </div>
		        <div class="col-md-6">
		         <label for="hora_i" type="button" class="btn btn-yellow "><strong><i class="fa fa-clock-o"></i></strong></label>
		       
		          <input id="hora_i" name="hora_inicio" type="text" class="validate timepicker" value="{{old('hora_inicio')}}" required placeholder ="Hora Inicio">
		          
		        </div>
		        <div class="col-md-6">
		        	 <label for="hora_fin" type="button" class="btn btn-yellow "><strong><i class="fa fa-clock-o"></i></strong></label>
		        	
		          <input id="hora_fin" name="hora_fin" type="text" class="validate timepicker" value="{{old('hora_fin')}}" required placeholder="Hora Final">
		          
		        </div>
		        <div class="col-md-12">
		        	 <label for="precio">Precio</label>
		          <input id="precio" name="precio" type="number" class="validate" value="{{old('precio')}}" required>
		         
		        </div>
		        <div class="col-md-12">
		        	<input type="submit" value="Guardar" class="btn btn-primary right waves-effect waves-light">
		        </div>
		        <p>&nbsp;</p><p>&nbsp;</p>
	    </div>
    </form>
  </div>



  @if($fechas)
	@foreach($fechas as $fecha)


	  <div id="info{{$fecha->id}}" class="modal">
	    <div class="modal-content">
	      <h5> Fecha Publicada para el Lugar ({{$fecha->nombre}}) </h5>
	      <div class="modal-body">
	      	<table class="table stirped">
	      		<tr >
	      			<th colspan="6" ><center>

	      				@if($fecha->status=='disponible')
	      				       <a class="waves-effect waves-light btn modal-trigger" href="#update{{$fecha->id}}" title="Editar Fecha"><i class="fa fa-edit"></i></a>
								<a class="waves-effect waves-light btn red modal-trigger" href="#delete{{$fecha->id}}" title="Eliminar Fecha"><i class="fa fa-times-circle"></i></a>
								
								<a class="waves-effect waves-light btn green modal-trigger" href="#reservaInterna" title="Rentar O Apartar Lugar" onclick="llenado({{$fecha->id}})" ><i class="fa fa-calendar"></i></a>
								@else

								<a class="waves-effect waves-light btn modal-trigger" href="#" title="Editar Fecha" disabled><i class="fa fa-edit"></i></a>
								<a class="waves-effect waves-light btn red modal-trigger" href="#" title="Eliminar Fecha" disabled><i class="fa fa-times-circle"></i></a>
								<a class="waves-effect waves-light btn green modal-trigger" href="#" title="Rentar O Apartar Lugar" disabled><i class="fa fa-calendar"></i></a>
									
								@endif
	      			</center></th>	      			
	      		</tr>
	      		<tr>
	      			<th>Fecha Publicada:</th>
	      			<td>{{ date("d/m/Y", strtotime($fecha->fecha))}}</td>
	      		
	      			<th>Hora Inicio:</th>
	      			<td>{{$fecha->hora_inicio}}</td>
	      	
	      			<th>Hora Final:</th>
	      			<td>{{$fecha->hora_fin}}</td>
	      		</tr>
	      		<tr>
	      		
	      			<th>Precio Publicado:</th>
	      			<th>{{number_format($fecha->precio,2,',','.')}} MXN</th>
	      		
	      			<th >Condición:</th>
	      			<td colspan="2">{{strtoupper($fecha->status)}} </td>
	      		</tr>
	      		
	      		@if($fecha->status=='apartado' || $fecha->status=='rentado')
	      		<tr>
	      			<th>Apartador:</th>
	      			<td colspan="4">Sr(a):{{$fecha->apartador}} </td>
	      		</tr>
	      		<tr>
	      			<th>Nota:</th>
	      			<td colspan="4">{{$fecha->nota}} </td>
	      		</tr>

	      		@endif

	      		<tr class="success">
	      			<th colspan="6" ><center>Solicitud de Apartado | Reservación</center></th>	      			
	      		</tr>


	      		
	      	</table>
			<div class="table-responsive">
			<table class="table stirped">
				<thead>
					
	      		<tr>
	      		<th>#</th>
	      		<th>cliente</th>
	      		<th>Fecha Solicitud</th>
	      		<th>Pago</th>
	      		<th>Estatus</th>
	      		</tr>
				</thead>
				<tbody>
				@foreach($fecha->reserva as $reserva)
				<tr>
					<td>{{$loop->iteration}}</td>
					<td>{{bcrypt($reserva->user->name)}}</td>
					<td>{{$reserva->created_at}}</td>
					<td>{{$reserva->formapago}}</td>
					<td>{{strtoupper($reserva->status)}}</td>

				</tr>

				@endforeach
				</tbody>
				
			</table>
		</div>
			<hr>

	      	<a href="#!" class="modal-action modal-close waves-effect waves-green btn" style="float: right;">Cancelar</a> 
	      </div>
	      

	    </div>


	  </div>
	<!-- Modal Structure -->
	  <div id="delete{{$fecha->id}}" class="modal">
	    <div class="modal-content">
	      <h4>Eliminar Fecha de ({{$fecha->nombre}})</h4>
	      <p>¿Está seguro que desea eliminar este La Fecha Seleccionada?</p>

	      <a href="#!" class="modal-action modal-close waves-effect waves-green btn" style="float: right;">Cancelar</a> 
			<form action="{{ url('/eliminar-fecha') }}" method="post" enctype="multipart/form-data">
				{{ method_field('DELETE') }}
				{!! csrf_field() !!}
				<input type="hidden" name="eliminar" value="{{$fecha->id}}" style="float: right;">
				<input type="submit" class="modal-action modal-close waves-effect waves-green red btn" value="Eliminar" style="float: right;">
			</form>
			<p>	&nbsp;</p>
	    </div>


	  </div>


<!-- Modal Structure -->
	  <div id="update{{$fecha->id}}" class="modal">
	  	<form action="{{ url('/actualizar-fecha') }}" method="post" enctype="multipart/form-data">
	    <div class="modal-content">
	      <h4>Editar ({{$fecha->nombre}} - {{$fecha->fecha}})</h4>
				{!! csrf_field() !!}

				<div class="col-md-12">
					<label for="venue{{$fecha->id}}">Lugar</label>
					<br>
		          <select name="venue_id" id="venue{{$fecha->id}}" class="select">
		        
		          	<option value="">Selecciona...</option>
		          	@foreach($venues as $venue)
						<option value="{{$venue->id}}">{{$venue->Titulo}}</option>
		          	@endforeach
		          </select>

		       
		          @foreach($venues as $venue)
						<script>
							console.log('{{$venue->id}}');
			          	document.getElementById('venue{{$fecha->id}}').value="{!!$venue->id or old('venue_id')!!}"
			          </script>
		          	@endforeach
		          
		          
		        </div>
				<div class="col-md-12">
					 <label for="fecha_e{{$fecha->id}}" type="button" class="btn btn-yellow "><strong><i class="fa fa-calendar"></i></strong></label>

		          <input id="fecha_e{{$fecha->id}}" name="fecha" type="text" class="validate datepicker" value="{{$fecha->fecha or old('fecha')}}" required>
		        
		        </div>
		        <div class="col-md-6">
		         <label for="hora_inicio{{$fecha->id}}" type="button" class="btn btn-yellow "><strong><i class="fa fa-clock-o"></i></strong></label>
		          <input id="hora_inicio{{$fecha->id}}" name="hora_inicio" type="text" class="validate timepicker" value="{{$fecha->hora_inicio or old('hora_inicio')}}" required>
		        
		        </div>
		        <div class="col-md-6">
		         <label for="hora_fin{{$fecha->id}}" type="button" class="btn btn-yellow "><strong><i class="fa fa-clock-o"></i></strong></label>
		          <input id="hora_fin{{$fecha->id}}" name="hora_fin" type="text" class="validate timepicker" value="{{$fecha->hora_fin or old('hora_fin')}}" required>
		          
		        </div>
		        <div class="col-md-12">
		         <label for="precio">Precio</label>
		          <input id="precio" name="precio" type="number" class="validate" value="{{$fecha->precio or old('precio')}}" required>
		         
		        </div>
		        <input type="hidden" name="id" value="{{$fecha->id}}">
		        <div class="col-md-12">
		        	<input type="submit" value="Guardar" class="btn btn-primary right waves-effect waves-light">
		        </div>
		        <p>&nbsp;</p><p>&nbsp;</p>
	    </div>

	    </form>
	  </div>

	  @endforeach
	@endif
 <div id="reservaInterna" class="modal">
	    <div class="modal-content">
	      <h4>Rentar o Apartar Lugar</h4>
	

	    
			<form action="{{ url('/reserva-directa') }}" method="post" novalidate="">
			
				{!! csrf_field() !!}

				<input type="hidden" name="id" id="vhoras" >
		        <div class="input-field col m12">
		        	<label for="fecha">Nombre del Cliente</label>
		          <input id="cliente" name="cliente" type="text" class="validate" value="{{old('cliente')}}" required>
		          
		        </div>

		         <div class="input-field col m12">
		          <label for="fecha">Nota de Apartado o Reserva:</label>
		        <textarea id="nota" name="nota" class="materialize-textarea" required>{{old('nota')}}</textarea>
		        
		        </div>


		        <div class="input-field col m12">
		          
		        <select name="condicion" id="condicion" class="select validate" required>
		        <option value="">SELECCIONE</option>
				<option value="apartado">APARTADO</option>
				<option value="rentado">RENTADO</option>
										
				</select> 
				<label for="zona">Condición:</label>
		        
		        </div>
				<div class="col-md-12" >
					<input type="submit" value="Guardar" class="btn btn-primary right waves-effect waves-light">

		         <a href="#!" class="modal-action modal-close waves-effect waves-green btn" style="float: right;">Cancelar</a>  
				</div>
			</form>
			<p>	&nbsp;</p>
	    </div>


	  </div>
