<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*usuarios */
Auth::routes();



/*- Jionic#2050*/
Route::get('/', function () {
	$venues=App\models\GestionEspacios::where('destacado', 1)->where('habilitado',true)->get();
	$zonas=App\Zona::orderBy('nombre','asc')->where('habilitado',true)->get();

    return view('inicio',compact('venues','zonas'));
})->name('home.index');
///gestion de espacios //
Route::resource('gestion_espacios','GestionEspaciosController');
Route::get('eliminar-img/{id}', 'GestionEspaciosController@destroyimg');
Route::get('lugares', 'GestionEspaciosController@buscar');
Route::get('destacar/{id}', 'GestionEspaciosController@destacar');
Route::get('showprov/{id}', 'GestionEspaciosController@showProv');
//gestion de espacios

Route::get('/perfil', function () {
	$user=App\User::find(Auth::user()->id);

	return view('perfil', ['user'=>$user]);  
  
})->middleware('auth')->name('Miperfil');

Route::get('user/{id}', 'UsuarioController@update');
Route::post('reset/{id}', 'UsuarioController@reset');
Route::get('/reservaClient', function () {
	$reserva=App\Reservacion::where('user_id',Auth::user()->id)->get();
	
	return view('cliente.reserva', compact('reserva'));
})->middleware('auth')->name('reserva.client');

Route::get('/vista-lugar/{id}', 'GestionEspaciosController@vistalugar')->middleware('auth')->name('vistalugarClient');




Route::post('/perfil/update-image',['as'=>'Miperfil-update-image','uses'=>'PerfilController@img_perfil']);

Route::get('activacion/{token}/{email}', 'HomeController@activacion');

Route::get('/terraza', function () {
    return view('terraza');
});

// Authentication routes...
Route::get('entrar', 'Auth\LoginController@showLoginForm');
Route::post('entrar', 'Auth\LoginController@login');

Route::get('acceso', 'RolesController@index');

Route::get('/salir', function () {
    Auth::logout();
    return redirect()->intended('/');
})->middleware('auth');

// Registration routes...
Route::get('registro', 'Auth\RegisterController@showRegistrationForm');
Route::post('registro', 'Auth\RegisterController@register');

// Facebook
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
Route::get('auth/facebook', 'Auth\LoginController@redirectToProvider');
Route::get('auth/facebook/retorno', 'Auth\LoginController@handleProviderCallback');
/*
Route::get('/lugares', function () {
	$lugares=App\Horario::orderBy('nombre','asc')->get();
	dd($lugares);
    return view('lugares',['lugares'=>$lugares]);
});*/




Route::get('/nuevo-proveedor', function () {
	$servicios=App\Servicio::orderBy('nombre','asc')->where('habilitado',true)->get();
	$zonas=App\Zona::orderBy('nombre','asc')->where('habilitado',true)->get();
    return view('auth.proveedores', ['servicios'=>$servicios,'zonas'=>$zonas]);
});
Route::post('nuevo-proveedor', 'ProveedoresController@store');


Route::get('/lugar/{id}', function ($id) {
	$fecha=now()->format('Y-m-d');
	$venue=App\Models\GestionEspacios::find($id);
	$horarios=App\Horario::where('venue_id',$id)->where('habilitado',true)->where('fecha','>=',$fecha)->get();	

	if($horarios){
		$min=$horarios->min('precio');
	}else{
		$min="-";
	}	
	
	if ($venue) {
		return view('terraza',compact('venue','min','horarios'));
	}
	else{
		return redirect()->intended(url('/404'));
	}
    
});

/*RESERVAR LUGAR*/
/**generar reservación*/



Route::get('reservar-lugar', 'GestionEspaciosController@reservar');
Route::get('apartar-lugar', 'ReservasController@apartar')->middleware('auth');
Route::post('reserva-directa', 'ReservasController@reserva_directa')->middleware('auth');
Route::get('reservadetalle', 'ReservasController@reservadetalle')->middleware('auth');
Route::get('reservavista', 'ReservasController@reservavista')->middleware('auth');

Route::post('pagar','OrdenController@pagar')->middleware('auth');
Route::get('confirmaReserva/{tokens}', 'ReservasController@completar')->middleware('auth');
Route::post('continuarPago','ReservasController@continuarPago')->middleware('auth');
Route::post('reservaEfectivo', 'ReservasController@efectivo')->middleware('auth');
Route::get('reservar', 'ReservasController@store')->middleware('auth');
Route::post('reserva/editar', 'ReservasController@editarReserva')->middleware('auth');
/*
modificacion momentanea*/
//Route::Resource('reservar','ReservasController')->middleware('auth');
/**/
Route::post('Openpay','OpenpayController@index');
Route::group(['middleware' => 'proveedor'], function(){

	Route::get('/panel', function () {

		$month = date('m');
		      $year = date('Y');
		      $day = date('d');
		      $from= date('Y-m-d', mktime(0,0,0, 1, 1, $year));
		      $day = date("d", mktime(0,0,0, 12, 31, $year+1));
		      $to = date('Y-m-d', mktime(0,0,0, 12, 31, $year));

			

			$totales= DB::table('reservacions')
    ->join('gestion_espacios', 'gestion_espacios.id', '=', 'reservacions.venue_id')    
    ->whereBetween('reservacions.created_at', array($from, $to)) 
    ->where('gestion_espacios.user_id',auth()->id())                 
    ->count();

    $rentadas= DB::table('reservacions')
    ->join('gestion_espacios', 'gestion_espacios.id', '=', 'reservacions.venue_id')    
    ->whereBetween('reservacions.created_at', array($from, $to)) 
    ->where('gestion_espacios.user_id',auth()->id())  
    ->where('status','rentado')               
    ->count();

    $espera= DB::table('reservacions')
    ->join('gestion_espacios', 'gestion_espacios.id', '=', 'reservacions.venue_id')    
    ->whereBetween('reservacions.created_at', array($from, $to)) 
    ->where('gestion_espacios.user_id',auth()->id())  
    ->where('status','apartado')               
    ->count();

			$vistos= DB::table('reservacions')
    ->join('gestion_espacios', 'gestion_espacios.id', '=', 'reservacions.venue_id')
    ->select('gestion_espacios.Titulo',DB::raw('count(reservacions.venue_id) as qty'))
    ->orderBy(DB::raw('count(reservacions.venue_id)'),'DESC')
    ->groupBy('gestion_espacios.Titulo','reservacions.venue_id')
    ->whereBetween('reservacions.created_at', array($from, $to)) 
    ->where('gestion_espacios.user_id',auth()->id()) 
    ->take(5)
    ->get();
    




			
	
		
	    	//return view('admin', ['usuarios'=>$usuarios,'mujeres'=>$mujeres,'hombres'=>$hombres,'from'=>$from,'to'=>$to]);
	    	return view('panel',compact('totales','rentadas','espera','from','to','vistos'));
	
	}); 

	Route::post('prov', 'HomeController@prov');
	Route::get('/venues', function () {
		$user=App\User::find(Auth::user()->id);
		$venues=$user->venues;
		$servicios=App\Servicio::orderBy('nombre','asc')->where('habilitado',true)->get();
		$zonas=App\Zona::orderBy('nombre','asc')->where('habilitado',true)->get();
	    return view('admin.venues', ['venues'=>$venues,'servicios'=>$servicios,'zonas'=>$zonas]);
	});
	Route::get('/venues/nuevo', function () {
		$servicios=App\Servicio::orderBy('nombre','asc')->where('habilitado',true)->get();
		$serviciosextra=App\ServicioExtra::where('user_id',Auth::user()->id)->orderBy('nombre','asc')->get();
		$zonas=App\Zona::orderBy('nombre','asc')->where('habilitado',true)->get();
	    return view('admin.venuenuevo', ['servicios'=>$servicios,'serviciosextra'=>$serviciosextra,'zonas'=>$zonas]);
	});
	Route::post('agregar-venue', 'VenueController@store');
	Route::delete('eliminar-venue', 'VenueController@destroy');

	Route::get('/venue/{id}', function ($id) {
		$venue=App\Venue::find($id);
		
		$servicios=App\Servicio::orderBy('nombre','asc')->where('habilitado',true)->get();
		$serviciosextra=App\ServicioExtra::where('user_id',Auth::user()->id)->orderBy('nombre','asc')->get();
		$zonas=App\Zona::orderBy('nombre','asc')->where('habilitado',true)->get();
		if ($venue) {
			return view('admin.actualizarvenue', ['servicios'=>$servicios,'venue'=>$venue,'serviciosextra'=>$serviciosextra,'zonas'=>$zonas]);
		}
		else{
			return redirect()->intended(url('/404'));

		}
	});
	Route::post('venue/{id}', 'VenueController@update');

	Route::get('/fechas', function () {
		$fechas=App\Horario::where('user_id',Auth::user()->id)->where('habilitado',true)->get();
		$venues=App\Models\GestionEspacios::where('user_id',Auth::user()->id)->where('aprobado',true)->get();
	
		/*$user=App\User::find(Auth::user()->id);
		$fechas=$user->horario;		
		$venues=$user->venues;*/

	   return view('admin.fechas',compact('fechas','venues'));
	});

	Route::get('/rentasConfirmar', function () {
		$reserva=App\Reservacion::where('condicion',3)->get();
		;
	
		
	   return view('admin.reservasinConfirmar',compact('reserva'));
	});


Route::get('calendar', 'HorarioController@api');
Route::post('agregar-fecha', 'HorarioController@store');
Route::post('actualizar-fecha', 'HorarioController@update');
Route::delete('eliminar-fecha', 'HorarioController@destroy');


Route::get('/servicios-extra', function () {
			$user=App\User::find(Auth::user()->id);
			$serviciosextra=$user->serviciosextra;
});

Route::post('agregar-servicioextra', 'ServicioController@storex');
Route::post('actualizar-servicioextra', 'ServicioController@updatex');
Route::delete('eliminar-servicioextra', 'ServicioController@destroyx');



});

Route::group(['middleware' => 'admin'], function(){

	Route::get('crm_usuarios', 'UsuarioController@crm_usuarios')->name('crm_usuarios');
	Route::get('crm_espera', 'UsuarioController@crm_espera')->name('crm_usuarios.espera');
    Route::get('crm_proveedores', 'UsuarioController@crm_proveedores')->name('crm_proveedores');

	Route::get('/admin', function () {
			$month = date('m');
		      $year = date('Y');
		      $day = date('d');
		      $from= date('Y-m-d', mktime(0,0,0, 1, 1, $year));
		      $day = date("d", mktime(0,0,0, 12, 31, $year+1));
		      $to = date('Y-m-d', mktime(0,0,0, 12, 31, $year));

			
			$usuarios=App\User::whereBetween('created_at', array($from, $to))->where('is_admin',0)->where('status','Activo')->count();
			$mujeres=App\User::whereBetween('created_at', array($from, $to))->where('is_admin',0)->where('status','Activo')->where('genero','Femenino')->count();
			$hombres=App\User::whereBetween('created_at', array($from, $to))->where('is_admin',0)->where('status','Activo')->where('genero','Masculino')->count();
			$totales=App\Reservacion::whereBetween('created_at', array($from, $to))->where('habilitado',1)->count();
			$rentadas=App\Reservacion::whereBetween('created_at', array($from, $to))->where('habilitado',1)->where('status','rentado')->count();
			$espera=App\Reservacion::whereBetween('created_at', array($from, $to))->where('habilitado',1)->where('status','apartado')->count();

			$vistos= DB::table('reservacions')
    ->join('gestion_espacios', 'gestion_espacios.id', '=', 'reservacions.venue_id')
    ->select('gestion_espacios.Titulo',DB::raw('count(reservacions.venue_id) as qty'))
    ->orderBy(DB::raw('count(reservacions.venue_id)'),'DESC')
    ->groupBy('gestion_espacios.Titulo','reservacions.venue_id')
    ->whereBetween('reservacions.created_at', array($from, $to))  
    ->take(5)                 
    ->get();



    $zona=DB::table('gestion_espacios')
    ->join('zonas', 'zonas.id', '=', 'gestion_espacios.zona_id')
    ->select('zonas.nombre',DB::raw('count(gestion_espacios.zona_id) as qty'))
    ->groupBy('gestion_espacios.zona_id','zonas.nombre')
    ->whereBetween('gestion_espacios.created_at', array($from, $to))  
    ->take(15)                 
    ->get();
	
		
	    	//return view('admin', ['usuarios'=>$usuarios,'mujeres'=>$mujeres,'hombres'=>$hombres,'from'=>$from,'to'=>$to]);
	    	return view('admin',compact('usuarios','mujeres','hombres','totales','rentadas','espera','from','to','vistos','zona'));

		});

	Route::post('admin', 'HomeController@admin');

	Route::get('/servicios', function () {
		$servicios=App\Servicio::orderBy('nombre','asc')->where('habilitado',true)->get();
	    return view('admin.servicios', ['servicios'=>$servicios]);
	});

	Route::post('agregar-servicio', 'ServicioController@store');
	Route::delete('eliminar-servicio', 'ServicioController@destroy');
	Route::post('actualizar-servicio', 'ServicioController@update');
	//Route::get('/peticiones','GestionEspaciosController@index');

	Route::get('/peticiones', function () {
		$peticiones=App\Models\GestionEspacios::with('zona','usuario')->where('aprobado',0)->orderBy('titulo','asc')->get();
	 return view('admin.peticiones', compact('peticiones'));
	});
	Route::delete('eliminar-peticion', 'ProveedoresController@destroy');	
	Route::post('aceptar-peticion', 'ProveedoresController@accept');


	Route::get('/zonas', function () {
		$zonas=App\Zona::orderBy('nombre','asc')->where('habilitado',true)->get();
	    return view('admin.zonas', ['zonas'=>$zonas]);
	});

	Route::post('agregar-zona', 'ZonaController@store');
	Route::delete('eliminar-zona', 'ZonaController@destroy');
	Route::post('actualizar-zona', 'ZonaController@update');


});


/*CARRITO PARA PEDIDOS*/
/*Funcionalidad del carrito*/
Route::get('/cart/show', 'CartController@show')->name('cart.show');//ver Carrito d compras
Route::get('/cart/add', 'CartController@add')->name('cart.add');//añadir items
Route::get('/cart/delete/{id}', 'CartController@delete')->name('cart.delete');//quitar items
Route::get('/cart/trash', 'CartController@trash')->name('cart.trash');//vaciar Carritos



//configuracion de rutas paypal
//envio de pedido paypal 

Route::get('/payment', 'PaypalController@postPayment');
Route::get('/payment/apartado/{tokens}', 'PaypalController@Paymentapartado');
Route::get('/payment/status', 'PaypalController@getPaymentStatus')->name('payment.status');
Route::get('/payment/statusv2', 'PaypalController@getPaymentStatusv2')->name('payment.statusv2');
Route::get('/payment/orderSave', 'PaypalController@Ordersave')->name('payment.orderSave')->middleware('auth');

Route::post('/payment/openpay', 'OpenpayController@index')->name('openpay.orderSave')->middleware('auth');

/*Rutas de Los Usuarios*/

Route::get('/verUser/{id}', 'UsuarioController@Veruser')->middleware('auth');
Route::get('/habilitar/{id}/{status}', 'UsuarioController@inactivar')->name('habilitar.user')->middleware('auth');
Route::get('/perfilprov', 'UsuarioController@editprov')->name('prov.user')->middleware('auth');

/*rutas de excel*/
Route::get('/crm_usuarios/excel', 'UsuarioController@excelUser')->name('crm_usuarios.excel');
Route::get('/crm_usuarios/proveedores', 'UsuarioController@excelProv')->name('crm_usuarios.excel');
/*Envio de Correo Electronico contacto*/

Route::post('/enviar', 'ContactoController@enviar')->name('envio.contacto');

