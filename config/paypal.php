<?php

return [

	//credenciales de paypal
	'client_id'=>env('PAYPAL_CLIENT_ID'),
	'secret'=>env('PAYPAL_SECRET'),

	/**
	ADK CONFIGURACIÓN
	**/

		'settings' => [
			/**
			*Avilable opcion sandbox or live
			*/

        'mode' => env('PAYPAL_MODE'),

        /**
        *Tiempo máximo de ejecución
        */
        'http.connectionTimeOut' => 60,

        'log.LogEnable'=>true,

        'log.FileName' =>storage_path().'/logs/paypal.log',

        'log.LogLevel'=>'FINE'
      ],




];
?>